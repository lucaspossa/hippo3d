# -*- coding: utf-8 -*-
'''
Canvas:
    Ligação das vigas nos pilares
    Copiar elementos de um pavimento para outro # nó ok ~ fazer pilar...
'''
from hippo_variaveis import np, pg, qg, _tr, qc, var

click_no = []
# click_2no = []
class HippoView(qg.QWidget):

    def __init__(self, nivel, indice):
        qg.QWidget.__init__(self)
        global click_no #, click_2no
        self.nivel = nivel
        self.indice = indice
        self.vb = pg.ViewBox()
        self.vb.setAspectLocked()
        self.grid = pg.GridItem()
        self.vb.addItem(self.grid)
        self.vb.setRange(yRange=(-100,500), xRange=(-100,500))
        self.view = pg.GraphicsView()
        self.view.setCentralItem(self.vb)
        self.cria_botoes()
# --------------------------------------------------------------------------- #
# ------------------------------ ###  VIGAS  ### ---------------------------- #
    def mais_viga(self):
        self.incid = []
        if var.elementoviga[self.indice] == []: self.dialogo_viga()
        # print 'oi'
        for i in var.gui_lista_nos[self.indice]:
            i.sigClicked.connect(chk_nos)
            print 'oi'
        self.view.scene().sigMouseClicked.connect(self.add_viga)

    def add_viga(self, ev):
        ev.currentItem = self.grid

        if len(self.incid) == 0:
            self.incid.append(click_no)
            print self.incid
        elif len(self.incid) == 1:
            tamanho = [float(var.elementoviga[self.indice][3]), float(var.elementoviga[self.indice][4])]
            self.incid.append(click_no)
            for i in var.gui_lista_nos[self.indice]:
                i.sigClicked.disconnect()
            print self.incid
            self.incid = []
            self.view.scene().sigMouseClicked.disconnect()
#            for i in var.gui_lista_pilar[self.indice]:
#                i.setMouseHover(True)
#                i.sigHoverEvent.connect(self.hoverEvent)
#                print 'aew'
            self.view.scene().sigMouseHover.connect(self.hoverEvent)
            print tamanho

        # else:
        #     coords = retorna_coordenadas(ev)
        #     no = pg.ScatterPlotItem(x=[coords[0]], y=[coords[1]], size=7.5, pxMode=False)
        #     var.gui_lista_nos[self.indice].append(no)
        # try:
        #     coords = tuple(coords - np.divide(tamanho, [2.,2.]))
        #     item = pg.ROI(coords, tuple(tamanho), movable=False)
        #     var.gui_lista_pilar[self.indice].append(item)
        # except: pass

        # self.plotar()

    def hoverEvent(self, ev):
        print ev
        if ev != []:
            print ev[0].pos()
#
##            if ev[0].isEnter() == True:
#            print 'aeo', ev[0].pos()

#        else: return
#        if ev.isEnter():
#            print 'oi'


    # def chk_2nos(self, plot, points):
    #     global click_2no
    #     if len(click_2no) == 0:
    #         click_2no.append([points[0].pos()[0], points[0].pos()[1]])
    #     elif len(click_2no) == 1:
    #         click_2no.append([points[0].pos()[0], points[0].pos()[1]])
    #     else: click_2no = []

    def dialogo_viga(self):
        self.diag_viga = qg.QDialog()
        # Botões ---------------------------------------------------------------
        cancelar = qg.QPushButton('Cancelar')
        self.connect(cancelar, qc.SIGNAL('clicked()'), self.diag_viga.close)
        aplicar = qg.QPushButton('Aplicar')
        self.connect(aplicar, qc.SIGNAL('clicked()'), self.viga_aplica)
        botao_ok = qg.QPushButton('OK')
        self.connect(botao_ok, qc.SIGNAL('clicked()'), self.viga_ok)
        tipo = qg.QLabel(_tr('Tipo de ambiente:'))
        self.cb_tipo_viga = qg.QComboBox()
        self.cb_tipo_viga.addItems(['Externo', 'Interno', 'Contato com solo'])
        vinc_viga = qg.QLabel(_tr('Vínculação:'))
        self.cb_vinc_viga = qg.QComboBox()
        self.cb_vinc_viga.addItems(['Engastado', 'Rotulado', _tr('Nó inicial'), _tr('Nó final')])
        cota = qg.QLabel(_tr('Elevação de nível (cm):'))
        self.le_cota_viga = qg.QLineEdit('0.0')
        dimensao_x = qg.QLabel(_tr('Dimensão X (cm):'))
        self.le_x_viga = qg.QLineEdit('20.0')
        dimensao_y = qg.QLabel(_tr('Dimensão Y (cm):'))
        self.le_y_viga = qg.QLineEdit('45.0')

        if var.elementoviga[self.indice] != []:
            self.cb_tipo_viga.setCurrentIndex(self.cb_tipo_viga.findText(var.elementoviga[self.indice][0], qc.Qt.MatchFixedString))
            self.cb_vinc_viga.setCurrentIndex(self.cb_vinc_viga.findText(var.elementoviga[self.indice][1], qc.Qt.MatchFixedString))
            self.le_cota_viga.setText(var.elementoviga[self.indice][2])
            self.le_x_viga.setText(var.elementoviga[self.indice][3])
            self.le_y_viga.setText(var.elementoviga[self.indice][4])
        else: self.viga_aplica()

        # Layout ---------------------------------------------------------------
        form = qg.QFormLayout()
        form.addRow(tipo, self.cb_tipo_viga)
        form.addRow(vinc_viga, self.cb_vinc_viga)
        form.addRow(cota, self.le_cota_viga)
        form.addRow(dimensao_x, self.le_x_viga)
        form.addRow(dimensao_y, self.le_y_viga)
        hbox = qg.QHBoxLayout()
        hbox.addWidget(cancelar)
        hbox.addWidget(aplicar)
        hbox.addWidget(botao_ok)
        vbox = qg.QVBoxLayout()
        vbox.addLayout(form)
        vbox.addLayout(hbox)
        self.diag_viga.setLayout(vbox)
        self.diag_viga.setWindowTitle("Viga")
        self.diag_viga.exec_()

    def viga_aplica(self):
        var.elementoviga[self.indice] = []
        tipo = self.cb_tipo_viga.currentText()
        vinc_viga = self.cb_vinc_viga.currentText()
        cota = self.le_cota_viga.text()
        x = self.le_x_viga.text()
        y = self.le_y_viga.text()
        # Lista da configuração do elemento de viga a ser inserido no canvas
        var.elementoviga[self.indice] = [tipo, vinc_viga, cota, x, y]

    def viga_ok(self):
        self.viga_aplica()
        self.diag_viga.close()
# --------------------------------------------------------------------------- #
# ----------------------------- ###  PILARES  ### --------------------------- #
    def mais_pilar(self):
        if var.elementopilar[self.indice] == []: self.dialogo_pilar()
        if var.elementopilar[self.indice][6] == 1:
            for i in var.gui_lista_nos[self.indice]:
                i.sigClicked.connect(chk_nos)
        self.view.scene().sigMouseClicked.connect(self.add_pilar)
#        self.view.scene().sigHoverEvent.connect(self.teste)
#        i.sigHoverEvent.connect(self.hoverEvent)

    def add_pilar(self, ev):
        ev.currentItem = self.grid
        tamanho = [float(var.elementopilar[self.indice][3]), float(var.elementopilar[self.indice][4])]
        if var.elementopilar[self.indice][6] == 1:
            coords = click_no
            for i in var.gui_lista_nos[self.indice]:
                i.sigClicked.disconnect()
        else:
            coords = retorna_coordenadas(ev)
            no = pg.ScatterPlotItem(x=[coords[0]], y=[coords[1]], size=7.5, pxMode=False)
            var.gui_lista_nos[self.indice].append(no)
        try:
            coords = tuple(coords - np.divide(tamanho, [2.,2.]))
            item = pg.ROI(coords, tuple(tamanho), movable=False)
            var.gui_lista_pilar[self.indice].append(item)
        except: pass
        self.view.scene().sigMouseClicked.disconnect()
        self.plotar()

    def dialogo_pilar(self):
        self.diag_pilar = qg.QDialog()
        # Botões ---------------------------------------------------------------
        cancelar = qg.QPushButton('Cancelar')
        self.connect(cancelar, qc.SIGNAL('clicked()'), self.diag_pilar.close)
        aplicar = qg.QPushButton('Aplicar')
        self.connect(aplicar, qc.SIGNAL('clicked()'), self.pilar_aplica)
        botao_ok = qg.QPushButton('OK')
        self.connect(botao_ok, qc.SIGNAL('clicked()'), self.pilar_ok)
        tipo = qg.QLabel(_tr('Tipo de ambiente:'))
        self.cb_tipo_pilar = qg.QComboBox()
        self.cb_tipo_pilar.addItems(['Externo', 'Interno', 'Contato com solo'])
        vinc_pilar = qg.QLabel(_tr('Vínculo no topo:'))
        self.cb_vinc_pilar = qg.QComboBox()
        self.cb_vinc_pilar.addItems(['Engastado', 'Rotulado', 'Momento em X', 'Momento em Y'])
        cota = qg.QLabel(_tr('Elevação de nível (cm):'))
        self.le_cota_pilar = qg.QLineEdit('0.0')
        dimensao_x = qg.QLabel(_tr('Dimensão X (cm):'))
        self.le_x_pilar = qg.QLineEdit('20.0')
        dimensao_y = qg.QLabel(_tr('Dimensão Y (cm):'))
        self.le_y_pilar = qg.QLineEdit('45.0')
        rotacao = qg.QLabel(_tr('Rotação (Deg):'))
        self.le_rotacao_pilar = qg.QLineEdit('0')
        insere = qg.QLabel(_tr('Inserir sobre nó:'))
        self.chk_pilar_no = qg.QCheckBox()

        if var.elementopilar[self.indice] != []:
            self.cb_tipo_pilar.setCurrentIndex(self.cb_tipo_pilar.findText(var.elementopilar[self.indice][0], qc.Qt.MatchFixedString))
            self.cb_vinc_pilar.setCurrentIndex(self.cb_vinc_pilar.findText(var.elementopilar[self.indice][1], qc.Qt.MatchFixedString))
            self.le_cota_pilar.setText(var.elementopilar[self.indice][2])
            self.le_x_pilar.setText(var.elementopilar[self.indice][3])
            self.le_y_pilar.setText(var.elementopilar[self.indice][4])
            self.le_rotacao_pilar.setText(var.elementopilar[self.indice][5])
            if var.elementopilar[self.indice][6] == 1:
                self.chk_pilar_no.setCheckState(qc.Qt.Checked)
            else: self.chk_pilar_no.setCheckState(qc.Qt.Unchecked)
        else: self.pilar_aplica()

        # Layout ---------------------------------------------------------------
        form = qg.QFormLayout()
        form.addRow(tipo, self.cb_tipo_pilar)
        form.addRow(vinc_pilar, self.cb_vinc_pilar)
        form.addRow(cota, self.le_cota_pilar)
        form.addRow(dimensao_x, self.le_x_pilar)
        form.addRow(dimensao_y, self.le_y_pilar)
        form.addRow(rotacao, self.le_rotacao_pilar)
        form.addRow(insere, self.chk_pilar_no)
        hbox = qg.QHBoxLayout()
        hbox.addWidget(cancelar)
        hbox.addWidget(aplicar)
        hbox.addWidget(botao_ok)
        vbox = qg.QVBoxLayout()
        vbox.addLayout(form)
        vbox.addLayout(hbox)
        self.diag_pilar.setLayout(vbox)
        self.diag_pilar.setWindowTitle(_tr("Pilar"))
        self.diag_pilar.exec_()

    def pilar_aplica(self):
        var.elementopilar[self.indice] = []
        tipo = self.cb_tipo_pilar.currentText()
        vinc_pilar = self.cb_vinc_pilar.currentText()
        cota = self.le_cota_pilar.text()
        x = self.le_x_pilar.text()
        y = self.le_y_pilar.text()
        r = self.le_rotacao_pilar.text()
        if self.chk_pilar_no.checkState() == qc.Qt.Checked:
            chk_no = 1
        else: chk_no = 0
        # Lista da configuração do elemento de fundação a ser inserido no canvas
        var.elementopilar[self.indice] = [tipo, vinc_pilar, cota, x, y, r, chk_no]

    def pilar_ok(self):
        self.pilar_aplica()
        self.diag_pilar.close()
# --------------------------------------------------------------------------- #
# ---------------------------- ###  FUNDAÇÃO  ### --------------------------- #
    def mais_fundacao(self):
        if var.elementofundacao[self.indice] == []: self.dialogo_fundacao()
        if var.elementofundacao[self.indice][7] == 1:
            for i in var.gui_lista_nos[self.indice]:
                i.sigClicked.connect(chk_nos)
        self.view.scene().sigMouseClicked.connect(self.add_fundacao)

    def add_fundacao(self, ev):
        ev.currentItem = self.grid
        tamanho = [float(var.elementofundacao[self.indice][4]), float(var.elementofundacao[self.indice][5])]
        if var.elementofundacao[self.indice][7] == 1:
            coords = click_no
            for i in var.gui_lista_nos[self.indice]:
                i.sigClicked.disconnect()
        else:
            coords = retorna_coordenadas(ev)
            no = pg.ScatterPlotItem(x=[coords[0]], y=[coords[1]], size=7.5, pxMode=False)
            var.gui_lista_nos[self.indice].append(no)
        try:
            coords = tuple(coords - np.divide(tamanho, [2.,2.]))
            item = pg.ROI(coords, tuple(tamanho), movable=False)
            var.gui_lista_fundacao[self.indice].append(item)
        except: pass
        self.view.scene().sigMouseClicked.disconnect()
        self.plotar()

    def dialogo_fundacao(self):
        self.diag_fundacao = qg.QDialog()
        # Botões ---------------------------------------------------------------
        cancelar = qg.QPushButton('Cancelar')
        self.connect(cancelar, qc.SIGNAL('clicked()'), self.diag_fundacao.close)
        aplicar = qg.QPushButton('Aplicar')
        self.connect(aplicar, qc.SIGNAL('clicked()'), self.fundacao_aplica)
        botao_ok = qg.QPushButton('OK')
        self.connect(botao_ok, qc.SIGNAL('clicked()'), self.fundacao_ok)
        tipo = qg.QLabel(_tr('Tipo de fundação:'))
        self.cb_tipo_fundacao = qg.QComboBox()
        self.cb_tipo_fundacao.addItems(['Sapata', 'Bloco', _tr('Tubulão')])
        vinc_pilar = qg.QLabel(_tr('Vínculo com pilar:'))
        self.cb_vinc_pilar = qg.QComboBox()
        self.cb_vinc_pilar.addItems(['Engastado', 'Rotulado', 'Momento em X', 'Momento em Y'])
        vinc_apoio = qg.QLabel(_tr('Vínculo com apoio:'))
        self.cb_vinc_apoio = qg.QComboBox()
        self.cb_vinc_apoio.addItems(['Engastado', 'Rotulado'])
        cota = qg.QLabel('Cota de rasamento (cm):')
        self.le_cota_fundacao = qg.QLineEdit('0.0')
        dimensao_x = qg.QLabel(_tr('Dimensão X (cm):'))
        self.le_x_fundacao = qg.QLineEdit('100.0')
        dimensao_y = qg.QLabel(_tr('Dimensão Y (cm):'))
        self.le_y_fundacao = qg.QLineEdit('100.0')
        altura = qg.QLabel(_tr('Altura útil (cm):'))
        self.le_h_fundacao = qg.QLineEdit('100.0')
        insere = qg.QLabel(_tr('Inserir sobre nó:'))
        self.chk_fundacao_no = qg.QCheckBox()

        if var.elementofundacao[self.indice] != []:
            self.cb_tipo_fundacao.setCurrentIndex(self.cb_tipo_fundacao.findText(var.elementofundacao[self.indice][0], qc.Qt.MatchFixedString))
            self.cb_vinc_pilar.setCurrentIndex(self.cb_vinc_pilar.findText(var.elementofundacao[self.indice][1], qc.Qt.MatchFixedString))
            self.cb_vinc_apoio.setCurrentIndex(self.cb_vinc_apoio.findText(var.elementofundacao[self.indice][2], qc.Qt.MatchFixedString))
            self.le_cota_fundacao.setText(var.elementofundacao[self.indice][3])
            self.le_x_fundacao.setText(var.elementofundacao[self.indice][4])
            self.le_y_fundacao.setText(var.elementofundacao[self.indice][5])
            self.le_h_fundacao.setText(var.elementofundacao[self.indice][6])
            if var.elementofundacao[self.indice][7] == 1:
                self.chk_fundacao_no.setCheckState(qc.Qt.Checked)
            else: self.chk_fundacao_no.setCheckState(qc.Qt.Unchecked)
        else: self.fundacao_aplica()

        # Layout ---------------------------------------------------------------
        form = qg.QFormLayout()
        form.addRow(tipo, self.cb_tipo_fundacao)
        form.addRow(vinc_pilar, self.cb_vinc_pilar)
        form.addRow(vinc_apoio, self.cb_vinc_apoio)
        form.addRow(cota, self.le_cota_fundacao)
        form.addRow(dimensao_x, self.le_x_fundacao)
        form.addRow(dimensao_y, self.le_y_fundacao)
        form.addRow(altura, self.le_h_fundacao)
        form.addRow(insere, self.chk_fundacao_no)
        hbox = qg.QHBoxLayout()
        hbox.addWidget(cancelar)
        hbox.addWidget(aplicar)
        hbox.addWidget(botao_ok)
        vbox = qg.QVBoxLayout()
        vbox.addLayout(form)
        vbox.addLayout(hbox)
        self.diag_fundacao.setLayout(vbox)
        self.diag_fundacao.setWindowTitle(_tr("Elemento de fundação"))
        self.diag_fundacao.exec_()

    def fundacao_aplica(self):
        var.elementofundacao[self.indice] = []
        tipo = self.cb_tipo_fundacao.currentText()
        vinc_pilar = self.cb_vinc_pilar.currentText()
        vinc_apoio = self.cb_vinc_apoio.currentText()
        cota = self.le_cota_fundacao.text()
        x = self.le_x_fundacao.text()
        y = self.le_y_fundacao.text()
        h = self.le_h_fundacao.text()
        if self.chk_fundacao_no.checkState() == qc.Qt.Checked:
            chk_no = 1
        else: chk_no = 0
        # Lista da configuração do elemento de fundação a ser inserido no canvas
        var.elementofundacao[self.indice] = [tipo, vinc_pilar, vinc_apoio, cota, x, y, h, chk_no]

    def fundacao_ok(self):
        self.fundacao_aplica()
        self.diag_fundacao.close()
# --------------------------------------------------------------------------- #
# ------------------------------- ###  NÓS  ### ----------------------------- #
    def mais_nos(self):
        x, ok = qg.QInputDialog.getText(self, _tr('Coordenadas do nó'), _tr('Insira a coordenada X:'))
        if ok:
            x = float(x)
        else: return
        y, ok = qg.QInputDialog.getText(self, _tr('Coordenadas do nó'), _tr('Insira a coordenada Y:'))
        if ok:
            y = float(y)
        else: return
        self.add_nos(x, y)

    def add_nos(self, x, y):
        item = pg.ScatterPlotItem(x=[x], y=[y], size=7.5, pxMode=False)
        var.gui_lista_nos[self.indice].append(item)
        self.plotar()

    def dialogo_nos(self):
        self.diag_nos = qg.QDialog()
        # Botões ---------------------------------------------------------------
        cancelar = qg.QPushButton('Cancelar')
        self.connect(cancelar, qc.SIGNAL('clicked()'), self.diag_nos.close)
        aplicar = qg.QPushButton('Aplicar')
        self.connect(aplicar, qc.SIGNAL('clicked()'), self.nos_aplica)
        botao_ok = qg.QPushButton('OK')
        self.connect(botao_ok, qc.SIGNAL('clicked()'), self.nos_ok)

        tipo = qg.QLabel(_tr(' - Restrições do nó - '))
        self.trans_x = qg.QCheckBox(_tr('Translação em X'))
        self.trans_y = qg.QCheckBox(_tr('Translação em Y'))
        self.rot_x = qg.QCheckBox(_tr('Rotação em X'))
        self.rot_y = qg.QCheckBox(_tr('Rotação em Y'))
        self.rot_z = qg.QCheckBox(_tr('Rotação em Z'))

        if var.elementono[self.indice] != []:
            if var.elementono[self.indice][0] == 1:
                self.trans_x.setCheckState(qc.Qt.Checked)
            else: self.trans_x.setCheckState(qc.Qt.Unchecked)
            if var.elementono[self.indice][1] == 1:
                self.trans_y.setCheckState(qc.Qt.Checked)
            else: self.trans_y.setCheckState(qc.Qt.Unchecked)
            if var.elementono[self.indice][2] == 1:
                self.rot_x.setCheckState(qc.Qt.Checked)
            else: self.rot_x.setCheckState(qc.Qt.Unchecked)
            if var.elementono[self.indice][3] == 1:
                self.rot_y.setCheckState(qc.Qt.Checked)
            else: self.rot_y.setCheckState(qc.Qt.Unchecked)
            if var.elementono[self.indice][4] == 1:
                self.rot_z.setCheckState(qc.Qt.Checked)
            else: self.rot_z.setCheckState(qc.Qt.Unchecked)
        else: self.nos_aplica()

        vbox = qg.QVBoxLayout()
        vbox.addWidget(tipo)
        vbox.addWidget(self.trans_x)
        vbox.addWidget(self.trans_y)
        vbox.addWidget(self.rot_x)
        vbox.addWidget(self.rot_y)
        vbox.addWidget(self.rot_z)
        vbox.addWidget(cancelar)
        vbox.addWidget(aplicar)
        vbox.addWidget(botao_ok)
        self.diag_nos.setLayout(vbox)
        self.diag_nos.setWindowTitle(_tr("Nó"))
        self.diag_nos.exec_()

    def nos_aplica(self):
        if var.elementono[self.indice] == []:
            self.trans_x.setCheckState(qc.Qt.Checked)
            self.trans_y.setCheckState(qc.Qt.Checked)
            self.rot_x.setCheckState(qc.Qt.Checked)
            self.rot_y.setCheckState(qc.Qt.Checked)
            self.rot_z.setCheckState(qc.Qt.Checked)
        var.elementono[self.indice] = []
        tx = 1 if self.trans_x.checkState() == qc.Qt.Checked else 0
        ty = 1 if self.trans_y.checkState() == qc.Qt.Checked else 0
        rx = 1 if self.rot_x.checkState() == qc.Qt.Checked else 0
        ry = 1 if self.rot_y.checkState() == qc.Qt.Checked else 0
        rz = 1 if self.rot_z.checkState() == qc.Qt.Checked else 0
        # Lista da configuração do elemento de nó a ser inserido no canvas -----
        var.elementono[self.indice] = [tx, ty, rx, ry, rz]

    def nos_ok(self):
        self.nos_aplica()
        self.diag_nos.close()
# --------------------------------------------------------------------------- #
# -------------- FUNÇÃO REPETIR ELEMENTOS PARA NÍVEL SUPERIOR --------------- #
    def repetir(self):
        self.diag_repetir = qg.QDialog()
        # Botões ---------------------------------------------------------------
        cancelar = qg.QPushButton('Cancelar')
        self.connect(cancelar, qc.SIGNAL('clicked()'), self.diag_repetir.close)
        aplicar = qg.QPushButton('Aplicar')
        self.connect(aplicar, qc.SIGNAL('clicked()'), self.repetir_aplica)
        botao_ok = qg.QPushButton('OK')
        self.connect(botao_ok, qc.SIGNAL('clicked()'), self.repetir_ok)

        lbl_repetir = qg.QLabel(_tr('Para repetir elementos desta planta ao nível superior,\nbasta selecionar o que deseja e clicar em ok.\nObservação: Não é possível repetir um pilar\nseparadamente do nó que o define.\nDesta forma, a repetição dos elementos\nse dá conforme a hierarquia escolhida.'))
        lbl_repetir.setAlignment(qc.Qt.AlignCenter)
        lbl_repetir2 = qg.QLabel(_tr('Não é possível realizar este procedimento caso\njá exista um elemento qualquer no nível superior.'))
        lbl_repetir2.setAlignment(qc.Qt.AlignCenter)
        lbl_combo = qg.QLabel(_tr('Itens a copiar:'))
        self.cb_repetir = qg.QComboBox()
        if self.indice == 0:
            self.cb_repetir.addItem(_tr('Nós'))
        else: self.cb_repetir.addItems([_tr('Nós'), 'Pilares', 'Vigas', 'Lajes'])

        hbox1 = qg.QHBoxLayout()
        hbox1.addWidget(lbl_combo)
        hbox1.addWidget(self.cb_repetir)
        hbox2 = qg.QHBoxLayout()
        hbox2.addWidget(cancelar)
        hbox2.addWidget(aplicar)
        hbox2.addWidget(botao_ok)
        vbox = qg.QVBoxLayout()
        vbox.addWidget(lbl_repetir)
        vbox.addWidget(qg.QLabel(''))
        vbox.addLayout(hbox1)
        vbox.addWidget(qg.QLabel(''))
        vbox.addWidget(lbl_repetir2)
        vbox.addWidget(qg.QLabel(''))
        vbox.addLayout(hbox2)
        self.diag_repetir.setLayout(vbox)
        self.diag_repetir.setWindowTitle(_tr("Repetir para nível superior"))
        self.diag_repetir.exec_()

    def repetir_aplica(self):
        if var.gui_lista_nos[self.indice+1] == []:
            item = self.cb_repetir.currentText()
        else: return

        if item == _tr('Nós'):
            self.rep_no()
        elif item == 'Pilares':
            self.rep_pilar()

    def rep_no(self):
        for i in var.gui_lista_nos[self.indice]:
            var.gui_lista_canvas[self.indice+1][0].add_nos(i.points()[0].pos()[0], i.points()[0].pos()[1])

    def rep_pilar(self):
        self.rep_no()
        pass
#        for i in var.gui_lista_pilar[self.indice]:
#            var.gui_lista_canvas[self.indice+1][0].add_nos(i.points()[0].pos()[0], i.points()[0].pos()[1])

    def repetir_ok(self):
        self.repetir_aplica()
        self.diag_repetir.close()
# --------------------------------------------------------------------------- #
# ------------- FUNÇÃO BASE PARA EXIBIÇÃO DOS BOTÕES NO CANVAS -------------- #
    def cria_botoes(self):
        self.bt_atualizar = qg.QPushButton('Atualizar')
        self.bt_atualizar.setFixedWidth(100)
        self.bt_atualizar.clicked.connect(self.plotar)
        self.bt_repetir = qg.QPushButton('Repetir acima')
        self.bt_repetir.setFixedWidth(100)
        self.bt_repetir.clicked.connect(self.repetir)
        # Cria nós -------------------------------------------------------------
        self.bt_dialogo_nos = qg.QPushButton(_tr('Nós'))
        self.bt_dialogo_nos.setFixedWidth(100)
        self.bt_dialogo_nos.clicked.connect(self.dialogo_nos)
        self.bt_m_nos = qg.QPushButton('+')
        self.bt_m_nos.setFixedWidth(25)
        self.bt_m_nos.clicked.connect(self.mais_nos)

        # Cria fundação --------------------------------------------------------
        self.bt_dialogo_fundacao = qg.QPushButton(_tr('Fundação'))
        self.bt_dialogo_fundacao.setFixedWidth(100)
        self.bt_dialogo_fundacao.clicked.connect(self.dialogo_fundacao)
        self.bt_m_fundacao = qg.QPushButton('+')
        self.bt_m_fundacao.setFixedWidth(25)
        self.bt_m_fundacao.clicked.connect(self.mais_fundacao)

        # Cria pilares ---------------------------------------------------------
        self.bt_dialogo_pilar = qg.QPushButton('Pilares')
        self.bt_dialogo_pilar.setFixedWidth(100)
        self.bt_dialogo_pilar.clicked.connect(self.dialogo_pilar)
        self.bt_m_pilar = qg.QPushButton('+')
        self.bt_m_pilar.setFixedWidth(25)
        self.bt_m_pilar.clicked.connect(self.mais_pilar)

        # Cria vigas -----------------------------------------------------------
        self.bt_dialogo_viga = qg.QPushButton('Vigas')
        self.bt_dialogo_viga.setFixedWidth(100)
        self.bt_dialogo_viga.clicked.connect(self.dialogo_viga)
        self.bt_m_viga = qg.QPushButton('+')
        self.bt_m_viga.setFixedWidth(25)
        self.bt_m_viga.clicked.connect(self.mais_viga)

        # Cria lajes -----------------------------------------------------------
        self.bt_dialogo_laje = qg.QPushButton('Lajes')
        self.bt_dialogo_laje.setFixedWidth(100)
        self.bt_m_laje = qg.QPushButton('+')
        self.bt_m_laje.setFixedWidth(25)

        # Seta layout no Widget ------------------------------------------------
        hbox = qg.QHBoxLayout()
        hbox.addWidget(self.bt_atualizar)
        hbox.addWidget(self.bt_dialogo_nos)
        hbox.addWidget(self.bt_m_nos)
        if self.indice == 0:
            hbox.addWidget(self.bt_dialogo_fundacao)
            hbox.addWidget(self.bt_m_fundacao)
        else:
            hbox.addWidget(self.bt_dialogo_pilar)
            hbox.addWidget(self.bt_m_pilar)
            hbox.addWidget(self.bt_dialogo_viga)
            hbox.addWidget(self.bt_m_viga)
            hbox.addWidget(self.bt_dialogo_laje)
            hbox.addWidget(self.bt_m_laje)
        hbox.addWidget(self.bt_repetir)
        hbox.addStretch()
        vbox = qg.QVBoxLayout()
        vbox.addWidget(self.view)
        vbox.addLayout(hbox)
        self.setLayout(vbox)
# --------------------------------------------------------------------------- #
# ----------------------- PLOTAR ELEMENTOS NO CANVAS ------------------------ #
    def plotar(self):
        self.vb.clear()
#        self.view.setCentralItem(self.vb)
        self.vb.addItem(self.grid)
        for i in var.gui_lista_nos[self.indice]:
            self.vb.addItem(i)
        for i in var.gui_lista_fundacao[self.indice]:
            self.vb.addItem(i)
        for i in var.gui_lista_pilar[self.indice]:
            self.vb.addItem(i)
        for i in var.gui_lista_viga[self.indice]:
            self.vb.addItem(i)
        for i in var.gui_lista_laje[self.indice]:
            self.vb.addItem(i)

#    def hoverEvent(self, ev):
#        if ev.isEnter():
#            print 'oi'
# --------------------------------------------------------------------------- #
'''
#        self.view.scene().sigMouseClicked.connect(self.onClick)
    def onClick(self, ev):
        ev.currentItem = self.grid
        coords = retorna_coordenadas(ev)
        if len(clicks)==0:  # First mouse click - ONLY register coordinates
            clicks.append((coords[0],coords[1]))
            print("First click!", clicks)
        elif len(clicks)==1:  # Second mouse click - register coordinates of second click
            clicks.append((coords[0],coords[1]))
            print("Second click...", clicks)
            # Draw line connecting the two clicks
            print("...drawing line")
            line = pg.LineSegmentROI(clicks, pen=(4,9), snapSize = 10., translateSnap=True, scaleSnap=True)
            self.vb.addItem(line)
            # reset clicks array
            clicks[:] = [] # this resets the *content* of clicks without changing the object itself
            self.view.scene().sigMouseClicked.disconnect()
        else: clicks[:] = []
'''
# --------------------------------------------------------------------------- #
def retorna_coordenadas(ev):
    x_pos = ev.pos().x()
    y_pos = ev.pos().y()
    xc = np.ceil(x_pos)
    xf = np.floor(x_pos)
    yc = np.ceil(y_pos)
    yf = np.floor(y_pos)
    x = xc if (xc-x_pos) < (x_pos-xf) else xf
    y = yc if (yc-y_pos) < (y_pos-yf) else yf
    return [x,y]

def chk_nos(plot, points):
    global click_no
    click_no = [points[0].pos()[0], points[0].pos()[1]]