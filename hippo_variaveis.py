# -*- coding: utf-8 -*-
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore as qc, QtGui as qg
pg.setConfigOptions(antialias=True) # Enable antialiasing for prettier plots
# ------------ FUNÇÕES BASE PARA EXIBIÇÃO DE TEXTO EM PORTUGUÊS ------------- #
try:
    _fromUtf8 = qc.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s
try:
    _encoding = qg.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return qg.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return qg.QApplication.translate(context, text, disambig)
def _tr(text):
    return _translate(None,text,None)
def gt(obj):
    try:
        res = obj.checkState()
    except:
        try:
            res = obj.currentText().encode('utf-8')
        except:
            try:
                res = obj.toPlainText().encode('utf-8')
            except:
                try:
                    res = obj.text().encode('utf-8')
                except:
                    res = None
    return str(res)
# --------------------------------------------------------------------------- #
# --------------------- CLASSE PARA CRIAÇÃO DE VARIÁVEIS -------------------- #
class Variaveis(object):
    def __init__(self):
        pass
    def cria_listas_gui(self):
        self.gui_lista_niveis = []
        self.gui_lista_canvas = []
        self.gui_lista_nos = [[],[],[],[],[],[],[],[],[],[]]
        self.gui_lista_fundacao = [[],[],[],[],[],[],[],[],[],[]]
        self.gui_lista_pilar = [[],[],[],[],[],[],[],[],[],[]]
        self.gui_lista_viga = [[],[],[],[],[],[],[],[],[],[]]
        self.gui_lista_laje = [[],[],[],[],[],[],[],[],[],[]]

    def cria_listas_config(self):
        self.elementono = [[],[],[],[],[],[],[],[],[],[]]
        self.elementofundacao = [[],[],[],[],[],[],[],[],[],[]]
        self.elementopilar = [[],[],[],[],[],[],[],[],[],[]]
        self.elementoviga = [[],[],[],[],[],[],[],[],[],[]]
        self.elementolaje = [[],[],[],[],[],[],[],[],[],[]]

var = Variaveis()
var.cria_listas_gui()
var.cria_listas_config()
# --------------------------------------------------------------------------- #