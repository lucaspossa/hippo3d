# -*- coding: utf-8 -*-
from sys import argv
from hippo_variaveis import qc, qg, _tr
from hippo_tree import HippoTree
# --------------------------------------------------------------------------- #
# ------------ CLASSE APPCAD PARA APRESENTAÇÃO INTERFACE GRÁFICA ------------ #
class HippoCAD(qg.QMainWindow):
    '''Interface gráfica para entrada e saída dos dados do CE3D'''
    def __init__(self, parent=None):
        # Janela principal -----------------------------------------------------
        qg.QMainWindow.__init__(self, parent)
        self.setWindowTitle(_tr('HIPPO: Cálculo Estrutural 3D'))
        self.setWindowState(qc.Qt.WindowMaximized)

        self.create_main_frame() # Define os entes da janela principal ---------

        # Botão Arquivo --------------------------------------------------------
        self.file_menu = qg.QMenu('&Arquivo', self)
        self.file_menu.addAction('&Sair', self.close, qc.Qt.CTRL + qc.Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)

# ---------- DEFINE OS ENTES DA JANELA PRINCIPAL->(qg.QMainWindow) ---------- #
    def create_main_frame(self):
        main_frame = qg.QWidget() # Cria janela para inserir entes -------------

        self.tree = HippoTree()
        self.mdi = qg.QMdiArea()
        self.tree.cria_mdi_cad(self.mdi)

        grid = qg.QGridLayout()
        grid.addWidget(self.tree,0,0)
        grid.addWidget(self.mdi,0,1)

        main_frame.setLayout(grid)
        self.setCentralWidget(main_frame) # Insere em (qg.QMainWindow) ---------
# --------------------------------------------------------------------------- #
# ------------------------------------ RODA --------------------------------- #
def main():
    app = qg.QApplication(argv)
    form = HippoCAD()
    form.show()
    app.exec_()
if __name__ == "__main__":
    main()