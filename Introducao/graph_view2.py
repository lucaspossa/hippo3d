# -*- coding: utf-8 -*-

'''
A classe GxBlock implementa um objeto gráfico que pode ser inserido na cena.
Em __init__() foram criados vários atributos, como o texto, sua cor e fonte,
assim como as dimensões do retângulo retornado por boundingRect(). O desenho
em si do objeto, que ocorre em paint() é bem straight-forward: pinta-se todo
o fundo, e então, desenha-se o texto tendo configurado apropriadamente uma
caneta (QPen) e uma fonte (QFont).
Quanto à main(), em relação ao protótipo mínimo, foram adicionadas algumas
flags para fazer uso de antialiasing, e, naturalmente, o GxBlock teve uma
instância adicionada à cena - observe que para isso basta passar a cena como
parâmetro na instanciação.
Vale notar também porquê os atributos definidos em __init__() são todos privados
(iniciam com "_"). Isso é porque, caso contrário, o usuário da classe poderia
pensar que fazendo gx_block.nome = 'New Name', o novo texto imediatamente
apareceria na tela - o que não é verdade (ver a próxima seção)

Para criar um objeto gráfico do zero, é preciso reimplementar QGraphicsItem.
Como trata-se de uma classe abstrata, é preciso lembrar que os métodos virtuais
boundingRect() e paint() precisam - obrigatoriamente - ser implementados.
'''

from PyQt4 import QtGui, QtCore
import sys

class GxBlock(QtGui.QGraphicsItem):
    def __init__(self, scene, parent=None):
        ''' (QGraphicsScene, QGraphicsItem) -> NoneType
        '''
        QtGui.QGraphicsItem.__init__(self, parent, scene)

        self._text = 'Hello World'
        self._text_font = QtGui.QFont('Verdana', 18)
        self._text_color = QtCore.Qt.white
        self._background_color = QtCore.Qt.blue

        self._width, self._height = 200, 50

    def boundingRect(self):
        ''' QGraphicsItem.boundingRect() -> QRectF
        '''
        return QtCore.QRectF(0, 0, self._width, self._height)

    def paint(self, painter, option=None, widget=None):
        ''' QGraphicsItem.paint(QPainter, QStyleOptionGrpahicsItem,
                                QWidget) -> NoneType
        '''
        painter.fillRect(self.boundingRect(), self._background_color)
        painter.setPen(QtGui.QPen(self._text_color))
        painter.setFont(self._text_font)
        painter.drawText(self.boundingRect(), QtCore.Qt.AlignCenter, 
                         self._text)

def main():
    app = QtGui.QApplication(sys.argv)
    win = QtGui.QMainWindow()
    win.setGeometry(200, 200, 640, 480)

    scene = QtGui.QGraphicsScene()
    scene.setSceneRect(0, 0, 1000, 1000)
    scene.setBackgroundBrush(QtGui.QColor(230, 230, 230))

    gx_block = GxBlock(scene)
    gx_block.setPos(50, 50)
    gx_block.setFlags(QtGui.QGraphicsItem.ItemIsMovable | 
                      QtGui.QGraphicsItem.ItemIsSelectable)

    view = QtGui.QGraphicsView(scene, win)
    view.setRenderHint(QtGui.QPainter.Antialiasing)
    view.setRenderHint(QtGui.QPainter.TextAntialiasing)
    view.centerOn(0, 0)

    win.setCentralWidget(view)
    win.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()