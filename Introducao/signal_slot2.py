# -*- coding: utf-8 -*-

'''
Uma solução elegante e simples em Python é utilizar funções lambda para criar
um wrapper em torno da chamada de função que se deseja fazer.

A introdução do método initUI() é uma prática bem comum em programas de PyQt
para caracterizar tudo que envolve a criação da interface em si, os widgets
e suas propriedades. Aqui ele basicamente chama setupGroupIcon() 9 vezes,
para criar cada um dos grupos combobox+label, e adicioná-los ao layout principal.

O método setupGroupIcon() retorna um layout contendo um combobox e seu
respectivo label alinhados verticalmente. A grande novidade aqui está na linha
45, onde é criada uma função lambda sem argumentos, cujo trabalho é chamar
changeColor() com o combobox e o label como parâmetros. Ou seja, ela atua como
um simples wrapper, que armazena as referências do combobox e label criados
nesta execução de setupGroupIcon().

O slot aqui é, portanto, changeColor(). A função é genérica, e atua em
quaisquer QComboBox e QLabel passados como parâmetro.

Observe, por fim, que os combobox e labels aqui inseridos NÃO tiveram
referências guardadas em atributos de MyWindow, e logo, seu acesso por
componentes externos fica dificultado. Para corrigir isso, poderia-se
acrescentar, no fim do método setupGroupIcon():
'''

from PyQt4 import QtGui, QtCore
import sys

class MyWindow(QtGui.QWidget):
    def __init__(self):
        ''' () -> MyWindow
        '''
        QtGui.QWidget.__init__(self)
        self.initUI()

    def initUI(self):
        ''' () -> NoneType

        Creates the user interface.
        '''
        h_layout = QtGui.QHBoxLayout()
        for i in range(0, 4):
            h_layout.addLayout(self.setupGroupIcon(i))

        self.setLayout(h_layout)

        self.setGeometry(200, 200, 0, 0)
        self.setWindowTitle('Signals and Slots - 2')

    def setupGroupIcon(self, i):
        ''' (int) -> QVBoxLayout

        Creates one group of combobox + label, and return the layout with them.
        '''
        combobox = QtGui.QComboBox(self)
        combobox.addItems(QtGui.QColor.colorNames())
        combobox.setFixedWidth(100)
        combobox.setCurrentIndex(-1)

        label_icon = QtGui.QLabel(self)
        label_icon.setFixedHeight(60)
        label_icon.setPalette(QtGui.QPalette())
        label_icon.setAutoFillBackground(True)
        


        self.connect(combobox, QtCore.SIGNAL('currentIndexChanged(int)'),
                     lambda: self.changeColor(combobox, label_icon))

        vl = QtGui.QVBoxLayout()
        vl.addWidget(combobox)
        vl.addWidget(label_icon)
        
        setattr(self, 'wg_cbx_%d' % i, combobox)
        setattr(self, 'wg_lb_icon_%d' % i, label_icon)
        
#        self.combo = getattr(self, 'wg_cbx_%d' %i)
#        print self.combo.

        return vl

    def changeColor(self, cbx, label):
        ''' (QComboBox, QLabel) -> NoneType

        Configures the background color of the label according to the current
        text of 'cbx'.
        '''
        palette = label.palette()
        palette.setColor(label.backgroundRole(),
                         QtGui.QColor(cbx.currentText()))
        label.setPalette(palette)
        
        print 'teste'
        for i in range (0,4):
            combo = getattr(self, 'wg_cbx_%d' %i)
            print combo
            lbl = getattr(self, 'wg_lb_icon_%d' %i)
            print lbl.text()

def main():
    app = QtGui.QApplication(sys.argv)
    win = MyWindow()
    win.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()