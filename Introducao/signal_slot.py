# -*- coding: utf-8 -*-
'''
Programar em PyQt significa, em geral, reimplementar uma classe disponível
(no caso QWidget), acrescentando-lhe novas funcionalidades.
Aqui a classe MyWindow foi criada com intuito apenas de encapsular tanto o
botão como o seu slot, já que ambos fazem parte da janela em si.

Um dos grandes atrativos de PyQt é o seu mecanismo de signals e slots, que
permite tratamento de eventos em altíssimo nível. Por exemplo, um evento de
"clique de mouse" propaga-se numa janela de PyQt até o filho correspondente
da solicitação, e então este filho responde com um signal dizendo "fui clicado!".
PyQt então verificará se há algum slot associado a este signal, executando-o se
a resposta for positiva.

O exemplo abaixo ilustra isso, onde foi disponibilizado um botão na tela e
programada uma ação para quando ele for clicado:
'''

from PyQt4 import QtGui, QtCore
import sys

class MyWindow(QtGui.QWidget):
    MSG_TITLE = 'Greetings'
    MSG_TEXT = \
        "Hello world!\n\nThe Great thing about PyQt4 is the "\
        "new signal/slot mechanism! Go check it out!" 

    def __init__(self):
        # inicializa sem parent (será uma janela)
        QtGui.QWidget.__init__(self)     

        # parametros: x, y, largura, altura
        self.setGeometry(200, 200, 500, 300)
        self.setWindowTitle('Hello (PyQt) world!')

        # cria um botão cujo parent é esta janela (self)
        self.btn_hello = QtGui.QPushButton('Say Hello', self)

        # conecta o sinal 'clicked()' com o método sayHello()
        self.connect(self.btn_hello, QtCore.SIGNAL('clicked()'),
                     self.sayHello)

    def sayHello(self):
        # parametros: ícone, título, menssagem, botões
        QtGui.QMessageBox(QtGui.QMessageBox.Information, self.MSG_TITLE,
                          self.MSG_TEXT, QtGui.QMessageBox.Ok).exec_()

def main():
    app = QtGui.QApplication(sys.argv)
    win = MyWindow()
    win.show()
    sys.exit(app.exec_())   

if __name__ == '__main__':
    main()