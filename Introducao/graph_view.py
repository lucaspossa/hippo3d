# -*- coding: utf-8 -*-

'''
Todos os objetos ligados a este framework iniciam com QGraphics, e o pilar
central é constituído pelos dois seguintes objetos:

QGraphicsScene: Define um container para itens gráficos. Não é QWidget-based,
e logo, não possui o método show(), não podendo ser visualizado na tela como
um widget. Sua função é realizar a manutenção dos itens.
QGraphicsView: É um QWidget-based que herda QAbstractScrollArea, sendo portanto
uma scroll area - i.e., uma view port (área visualizável) associada com scroll
bars (barras de rolagem) - especial para vizualizar o conteúdo de uma cena
definida por um QGraphicsScene.
Todo item gráfico "inserível" na cena herda de QGraphicsItem, uma classe
abstrata. Os dois métodos que se destacam nessa classe são:

boundingRect(): Retorna um objeto QRectF, que define um retângulo que circunda
o item gráfico. Requisições para repintar o item, assim como detecção de colisões
se baseiam nesse retângulo.
paint(): Possui o parâmetro painter, que é um objeto QPainter. Todos os desenhos
feitos nesse método usando painter utilizam as coordenadas dentro do item - i.e.,
a origem é definida pelo retângulo de boundingRect(). Essa origem é mapeada para
cena no atributo pos() do item, e o mapeamento dos pontos relativos fornecidos
para o painter é automática. Por fim, se você desenhar fora do boundingRect(),
a área excedente não será repintada em requisições de update.
'''

from PyQt4 import QtGui, QtCore
import sys

def main():
    app = QtGui.QApplication(sys.argv)
    win = QtGui.QMainWindow()
    win.setGeometry(200, 200, 500, 300)

    scene = QtGui.QGraphicsScene()
    scene.setSceneRect(0, 0, 1000, 1000)
    scene.setBackgroundBrush(QtGui.QColor(230, 230, 230))

    view = QtGui.QGraphicsView(scene, win)    

    win.setCentralWidget(view)
    win.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
    
'''
Apesar de QGraphicsView ser QWidget-based, observa-se na prática um comportamento
estranho quando ele é feito de top-level, ou até mesmo, quando o top-level é um
próprio QWidget. Daí a necessidade de se criar um QMainWindoow aqui.
De resto, a idéia é bem simples: temos uma scene de 1000 por 1000 pixels,
enquadrada por view, cujo tamanho é determinado pelo seu parent, win - sendo
portanto, de 500 por 300 pixels.
Vale lembrar que uma mesma cena pode ser visualizada por diversos views.
'''