# -*- coding: utf-8 -*-
import numpy as np

class LineBuilder:

    def __init__(self, line):
        self.line = line
        self.xs = list(line.get_xdata())
        self.ys = list(line.get_ydata())
        self.xs_ = list(line.get_xdata())
        self.ys_ = list(line.get_ydata())
        self.liga0 = line.figure.canvas.mpl_connect('button_press_event', self)

    def __call__(self, event):

        self.xs.append(event.xdata)
        self.ys.append(event.ydata)

        xc = np.ceil(self.xs[-1])
        xf = np.floor(self.xs[-1])
        yc = np.ceil(self.ys[-1])
        yf = np.floor(self.ys[-1])

        if (xc-self.xs[-1]) < (self.xs[-1]-xf):
            self.xs_.append(xc)
        else:
            self.xs_.append(xf)

        if (yc-self.ys[-1]) <(self.ys[-1]-yf):
            self.ys_.append(yc)
        else:
            self.ys_.append(yf)

        self.line.set_data(self.xs_,self.ys_)
        self.line.figure.canvas.draw()

        print('click',self.xs_[-1], self.ys_[-1])
        
#self.line, = self.axes.plot([], [], c='#666666', ls='-', marker='x', mew=2, mec='#204a87')
#self.linebuilder = LineBuilder(self.line)