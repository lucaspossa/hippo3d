# -*- coding: utf-8 -*-

from hippo_variaveis import qc, np, pg

class HippoCanvas(pg.GraphicsWindow):
    ''' Canvas & ViewBox pyqtgraph '''
    def __init__(self, nivel):
        pg.GraphicsWindow.__init__(self, None)
        
        self.view_box = pg.ViewBox()
        self.addItem(self.view_box)
        self.view_box.setAspectLocked()
        self.view_box.addItem(pg.GridItem())

        self.graph_theory = Graph()
        self.view_box.addItem(self.graph_theory)

        ## Define positions of nodes
        pos = np.array([
            [0,0],
            [10,0],
            [0,10],
            [10,10],
            [5,5],
            [15,5]
            ], dtype=float)
        
        ## Define the set of connections in the graph
        adj = np.array([
            [0,1],
            [1,3],
            [3,2],
            [2,0],
            [1,5],
            [3,5],
            ])
        
        ## Define the symbol to use for each node (this is optional)
        symbols = ['o','o','o','o','t','+']
        
        ## Define the line style for each connection (this is optional)
        lines = np.array([
            (255,0,0,255,1),
            (255,0,255,255,2),
            (255,0,255,255,3),
            (255,255,0,255,2),
            (255,0,0,255,1),
            (255,255,255,255,4),
            ], dtype=[('red',np.ubyte),('green',np.ubyte),('blue',np.ubyte),('alpha',np.ubyte),('width',float)])
        
        ## Define text to show next to each symbol
        texts = ["Point %d" % i for i in range(6)]
        
        ## Update the graph
        #def up_graph():
        self.graph_theory.setData(pos=pos, adj=adj, pen=lines, size=1, symbol=symbols, pxMode=False, text=texts)
        
        pos2 = np.array([
            [0,0],
            [5,0],
            [0,5],
            [5,5]
            ], dtype=float)
    
        adj2 = np.array([
            [0,1],
            [1,2],
            [3,2],
            [2,0],
            [3,0],
            ])
    
        texts2 = ["Point %d" % i for i in range(4)]
        
        self.graph_theory2 = Graph()
        self.view_box.addItem(self.graph_theory2)
        
        self.graph_theory2.setData(pos=pos2, adj=adj2, size=1, pxMode=False, text=texts2)
        
        r3a = pg.ROI([2.5,2.5], [5,5])
        self.view_box.addItem(r3a)
#        ## handles scaling horizontally around center
        r3a.addScaleHandle([1, 0.5], [0.5, 0.5])
        r3a.addScaleHandle([0, 0.5], [0.5, 0.5])
        
#        ## handles scaling vertically from opposite edge
        r3a.addScaleHandle([0.5, 0], [0.5, 1])
        r3a.addScaleHandle([0.5, 1], [0.5, 0])
#        
        ## handles scaling both vertically and horizontally
        r3a.addScaleHandle([1, 1], [0, 0])
        r3a.addScaleHandle([0, 0], [1, 1])
        
class Graph(pg.GraphItem):
    def __init__(self):
        self.dragPoint = None
        self.dragOffset = None
        self.textItems = []
        pg.GraphItem.__init__(self)
        self.scatter.sigClicked.connect(self.clicked)

    def setData(self, **kwds):
        self.text = kwds.pop('text', [])
        self.data = kwds
        if 'pos' in self.data:
            npts = self.data['pos'].shape[0]
            self.data['data'] = np.empty(npts, dtype=[('index', int)])
            self.data['data']['index'] = np.arange(npts)
        self.setTexts(self.text)
        self.updateGraph()

    def setTexts(self, text):
        for i in self.textItems:
            i.scene().removeItem(i)
        self.textItems = []
        for t in text:
            item = pg.TextItem(t)
            self.textItems.append(item)
            item.setParentItem(self)

    def updateGraph(self):
        pg.GraphItem.setData(self, **self.data)
        for i,item in enumerate(self.textItems):
            item.setPos(*self.data['pos'][i])


    def mouseDragEvent(self, ev):
        if ev.button() != qc.Qt.LeftButton:
            ev.ignore()
            return

        if ev.isStart():
            # We are already one step into the drag.
            # Find the point(s) at the mouse cursor when the button was first
            # pressed:
            pos = ev.buttonDownPos()
            pts = self.scatter.pointsAt(pos)
            if len(pts) == 0:
                ev.ignore()
                return
            self.dragPoint = pts[0]
            ind = pts[0].data()[0]
            self.dragOffset = self.data['pos'][ind] - pos
        elif ev.isFinish():
            self.dragPoint = None
            return
        else:
            if self.dragPoint is None:
                ev.ignore()
                return

        ind = self.dragPoint.data()[0]
        self.data['pos'][ind] = ev.pos() + self.dragOffset
        self.updateGraph()
        ev.accept()

    def clicked(self, pts, ev):
        print("clicked: %s" % pts)
        
        print ev[0].pos()

#        if ev.isStart():
#            print self.scatter.pointsAt(ev.buttonDownPos())