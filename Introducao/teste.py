# -*- coding: utf-8 -*-
"""
Simple example of GraphItem use.
"""


#import initExample ## Add path to library (just for examples; you do not need this)
#
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np

# Enable antialiasing for prettier plots
pg.setConfigOptions(antialias=True)

w = pg.GraphicsWindow()
w.setWindowTitle('pyqtgraph example: GraphItem')

#v2 = w.add1

v = w.addViewBox()
v.setAspectLocked()

v2 = w.addViewBox()

g = pg.GraphItem()
v.addItem(g)

## Define positions of nodes
pos = np.array([
    [0,0],
    [10,0],
    [0,10],
    [10,10],
    [5,5],
    [15,5]
    ])

## Define the set of connections in the graph
adj = np.array([
    [0,1],
    [1,3],
    [3,2],
    [2,0],
    [1,5],
    [3,5],
    ])

## Define the symbol to use for each node (this is optional)
symbols = ['o','o','o','o','t','+']

## Define the line style for each connection (this is optional)
lines = np.array([
    (255,0,0,255,1),
    (255,0,255,255,2),
    (255,0,255,255,3),
    (255,255,0,255,2),
    (255,0,0,255,1),
    (255,255,255,255,4),
    ], dtype=[('red',np.ubyte),('green',np.ubyte),('blue',np.ubyte),('alpha',np.ubyte),('width',float)])

## Update the graph
g.setData(pos=pos, adj=adj, pen=lines, size=1, symbol=symbols, pxMode=False)




## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()



'''
from PyQt4 import QtGui  # (the example applies equally well to PySide)
import pyqtgraph as pg

## Always start by initializing Qt (only once per application)
app = QtGui.QApplication([])

## Define a top-level widget to hold everything
w = QtGui.QWidget()

## Create some widgets to be placed inside
btn = QtGui.QPushButton('press me')
text = QtGui.QLineEdit('enter text')
listw = QtGui.QListWidget()
plot = pg.PlotWidget()

## Create a grid layout to manage the widgets size and position
layout = QtGui.QGridLayout()
w.setLayout(layout)

## Add widgets to the layout in their proper positions
layout.addWidget(btn, 0, 0)   # button goes in upper-left
layout.addWidget(text, 1, 0)   # text edit goes in middle-left
layout.addWidget(listw, 2, 0)  # list widget goes in bottom-left
layout.addWidget(plot, 0, 1, 3, 1)  # plot goes on right side, spanning 3 rows

## Display the widget as a new window
w.show()

## Start the Qt event loop
app.exec_()
'''

'''
"""
Demonstrates some customized mouse interaction by drawing a crosshair that follows
the mouse.


"""

#import initExample ## Add path to library (just for examples; you do not need this)
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
from pyqtgraph.Point import Point

#generate layout
app = QtGui.QApplication([])
win = pg.GraphicsWindow()
win.setWindowTitle('pyqtgraph example: crosshair')
label = pg.LabelItem(justify='right')
win.addItem(label)
p1 = win.addPlot(row=1, col=0)
p2 = win.addPlot(row=2, col=0)

region = pg.LinearRegionItem()
region.setZValue(10)
# Add the LinearRegionItem to the ViewBox, but tell the ViewBox to exclude this
# item when doing auto-range calculations.
p2.addItem(region, ignoreBounds=True)

#pg.dbg()
p1.setAutoVisible(y=True)


#create numpy arrays
#make the numbers large to show that the xrange shows data from 10000 to all the way 0
data1 = 10000 + 15000 * pg.gaussianFilter(np.random.random(size=10000), 10) + 3000 * np.random.random(size=10000)
data2 = 15000 + 15000 * pg.gaussianFilter(np.random.random(size=10000), 10) + 3000 * np.random.random(size=10000)

p1.plot(data1, pen="r")
p1.plot(data2, pen="g")

p2.plot(data1, pen="w")

def update():
    region.setZValue(10)
    minX, maxX = region.getRegion()
    p1.setXRange(minX, maxX, padding=0)

region.sigRegionChanged.connect(update)

def updateRegion(window, viewRange):
    rgn = viewRange[0]
    region.setRegion(rgn)

p1.sigRangeChanged.connect(updateRegion)

region.setRegion([1000, 2000])

#cross hair
vLine = pg.InfiniteLine(angle=90, movable=False)
hLine = pg.InfiniteLine(angle=0, movable=False)
p1.addItem(vLine, ignoreBounds=True)
p1.addItem(hLine, ignoreBounds=True)


vb = p1.vb

def mouseMoved(evt):
    pos = evt[0]  ## using signal proxy turns original arguments into a tuple
    if p1.sceneBoundingRect().contains(pos):
        mousePoint = vb.mapSceneToView(pos)
        index = int(mousePoint.x())
        if index > 0 and index < len(data1):
            label.setText("<span style='font-size: 12pt'>x=%0.1f,   <span style='color: red'>y1=%0.1f</span>,   <span style='color: green'>y2=%0.1f</span>" % (mousePoint.x(), data1[index], data2[index]))
        vLine.setPos(mousePoint.x())
        hLine.setPos(mousePoint.y())



proxy = pg.SignalProxy(p1.scene().sigMouseMoved, rateLimit=60, slot=mouseMoved)
#p1.scene().sigMouseMoved.connect(mouseMoved)


## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
'''
'''import random
import time
from collections import deque
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import os
#import spidev

win = pg.GraphicsWindow()
win.setWindowTitle('DOTS')


p1 = win.addPlot()
p1.setRange(yRange=[0,25])
p1.setRange(xRange=[0,25])
curve1 = p1.plot()


nsamples=300 #Number of lines for the data

dataRed= np.zeros((nsamples,2),float) #Matrix for the Red dots
dataBlue=np.zeros((nsamples,2),float) #Matrix for the Blue dots

def getData():
    global dataRed, dataBlue

    t0= random.uniform(1.6,20.5) #Acquiring Data
    d0= random.uniform(1.6,20.5) #Acquiring Data
    vec=(t0, d0)

    dataRed[:-1] = dataRed[1:]
    dataRed[-1]=np.array(vec)

    t0= random.uniform(1.6,20.5) #Acquiring Data
    d0= random.uniform(1.6,20.5) #Acquiring Data
    vec=(t0, d0)

    dataBlue[:-1] = dataBlue[1:]
    dataBlue[-1]=np.array(vec)


def plot():

    #Blue Dots
    curve1.setData(dataBlue, pen=None, symbol='o', symbolPen=None, symbolSize=4, symbolBrush=('b'))
    #Red Dots
    curve1.setData(dataRed, pen=None, symbol='o', symbolPen=None, symbolSize=4, symbolBrush=('r'))


def update():

    getData()
    plot()

timer = pg.QtCore.QTimer()
timer.timeout.connect(update)
timer.start(50)

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()# -*- coding: utf-8 -*-


'''
''' init__
#        self.file_menu.addAction('&Novo', self.Novo, qc.Qt.CTRL + qc.Qt.Key_N)
#        self.file_menu.addAction('Novo')
#        self.file_menu.addAction('Cascata')
#        self.file_menu.addAction('Organizar')
#        self.file_menu.triggered[qg.QAction].connect(self.windowaction)

#    def windowaction(self, q):
#        if q.text() == 'Novo':
#            sub = qg.QMdiSubWindow()
#            canvas = HippoCanvas()
#            sub.setWidget(canvas)
#            self.mdi.addSubWindow(sub)
#            sub.show()
#
#        if q.text() == 'Cascata':
#            self.mdi.cascadeSubWindows()
#
#        if q.text() == 'Organizar':
#            self.mdi.tileSubWindows()


'''
#import sys
#import pyqtgraph as pg
#from pyqtgraph.Qt import QtCore as qc, QtGui as qg
#
#class Teste(qg.QMainWindow):
#    '''Interface gráfica para entrada e saída dos dados do CE3D'''
#    def __init__(self, parent=None):
#        # Janela principal ----------------------------------------------------
#        qg.QMainWindow.__init__(self, parent)
#        self.setWindowTitle('oie')
#        self.setWindowState(qc.Qt.WindowMaximized)
#
#    def create_main_frame(self):
#        main_frame = qg.QWidget() # Cria janela para inserir entes ------------
#        self.create_canvas()
#
#        layout = qg.QHBoxLayout()
#        layout.addWidget(self.canvas)
#        main_frame.setLayout(layout)
#        self.setCentralWidget(main_frame) # Insere em (qg.QMainWindow) --------
#
#    def create_canvas(self):
#        self.canvas = pg.PlotWidget()
#        self.canvas.
#
#
#def main():
#    app = qg.QApplication(sys.argv)
#    form = Teste()
#    form.show()
#    app.exec_()
#if __name__ == "__main__":
#    main()
'''
"""
ViewBox is the general-purpose graphical container that allows the user to
zoom / pan to inspect any area of a 2D coordinate system.

This unimaginative example demonstrates the constrution of a ViewBox-based
plot area with axes, very similar to the way PlotItem is built.
"""
import sys
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore as qc, QtGui as qg

class Grafico(pg.GraphicsView):

    clicks = []

    def onClick(self, ev):
        global clicks
        x = ev.pos().x()
        y = ev.pos().y()
        if len(clicks)==0:  # First mouse click - ONLY register coordinates
            print("First click!")
            clicks.append((x,y))
        elif len(clicks)==1:  # Second mouse click - register coordinates of second click
            print("Second click...")
            clicks.append((x,y))

            # Draw line connecting the two clicks
            print("...drawing line")
            line = pg.LineSegmentROI(clicks, pen=(4,9))
            self.vb.addItem(line)

            # reset clicks array
            clicks[:] = [] # this resets the *content* of clicks without changing the object itself
        else:  # something went wrong, just reset clicks
            clicks[:] = []

    def __init__(self):
        pg.GraphicsView.__init__(self, None)

        self.mouseEnabled = True
        self.clickAccepted = True

        self.vb = pg.ViewBox()

        self.addItem(self.vb)

#        self.sigMouseReleased.connect(self.onClick)

#        self.vb.connect
#        self.set

#        self.addItem(self.vb)
#        self.scene.sigClicked(self.onClick)

#        self.scene = pg.GraphicsScene(parent=self)
#        self.setScene(self.scene)
#
##        self.sceneObj()
##
#        self.scene.sigMouseClicked.connect(self.onClick)
        self.scene().sigMouseClicked.connect(self.onClick)



def main():
    app = qg.QApplication(sys.argv)
    win = qg.QMainWindow()
    win.setWindowState(qc.Qt.WindowMaximized)
    win.setWindowTitle('Cena do add')
#    wdgt = qg.QWidget()
    grafico = Grafico()
#    layout = qg.QHBoxLayout()
#    layout.addWidget(grafico)

    win.setCentralWidget(grafico)
    win.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()'''
#'''
##
#
##
##
#

#import sys
#from PyQt4.QtGui import *
#from PyQt4.QtCore import *
#import pyqtgraph as pg
#import time
#import numpy as np
#
#class Screen(QMainWindow):
#    def __init__(self):
#        super(Screen, self).__init__()
#        self.initUI()
#
#    def initUI(self):
#        self.x = np.array([1,2,3,4])
#        self.y = np.array([1,4,9,16])
#        self.plt = pg.PlotWidget()
#        self.plot = self.plt.plot(self.x, self.y)
#
#        addBtn = QPushButton('Add Datapoint')
#        addBtn.clicked.connect(self.addDataToPlot)
#        addBtn.show()
#
#        mainLayout = QVBoxLayout()
#        mainLayout.addWidget(addBtn)
#        mainLayout.addWidget(self.plt)
#
#        self.mainFrame = QWidget()
#        self.mainFrame.setLayout(mainLayout)
#        self.setCentralWidget(self.mainFrame)
#
#    def addDataToPlot(self):
#        data = {
#            'x': 5,
#            'y': 25
#        }
#        self.x = np.append(self.x, data['x'])
#        self.y = np.append(self.y, data['y'])
#        self.plot.setData(self.x, self.y)
#
#
#app = QApplication(sys.argv)
#window = Screen()
#window.show()
#sys.exit(app.exec_())

''' teste bacana @@
"""
Demonstrates adding a custom context menu to a GraphicsItem
and extending the context menu of a ViewBox.

PyQtGraph implements a system that allows each item in a scene to implement its
own context menu, and for the menus of its parent items to be automatically
displayed as well.

"""
#import initExample ## Add path to library (just for examples; you do not need this)

import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np

win = pg.GraphicsWindow()
win.setWindowTitle('pyqtgraph example: context menu')


view = win.addViewBox()

# add two new actions to the ViewBox context menu:
zoom1 = view.menu.addAction('Zoom to box 1')
zoom2 = view.menu.addAction('Zoom to box 2')

# define callbacks for these actions
def zoomTo1():
    # note that box1 is defined below
    view.autoRange(items=[box1])
zoom1.triggered.connect(zoomTo1)

def zoomTo2():
    # note that box1 is defined below
    view.autoRange(items=[box2])
zoom2.triggered.connect(zoomTo2)



class MenuBox(pg.GraphicsObject):
    """
    This class draws a rectangular area. Right-clicking inside the area will
    raise a custom context menu which also includes the context menus of
    its parents.
    """
    def __init__(self, name):
        self.name = name
        self.pen = pg.mkPen('r')

        # menu creation is deferred because it is expensive and often
        # the user will never see the menu anyway.
        self.menu = None

        # note that the use of super() is often avoided because Qt does not
        # allow to inherit from multiple QObject subclasses.
        pg.GraphicsObject.__init__(self)


    # All graphics items must have paint() and boundingRect() defined.
    def boundingRect(self):
        return QtCore.QRectF(0, 0, 10, 10)

    def paint(self, p, *args):
        p.setPen(self.pen)
        p.drawRect(self.boundingRect())


    # On right-click, raise the context menu
    def mouseClickEvent(self, ev):
        if ev.button() == QtCore.Qt.RightButton:
            if self.raiseContextMenu(ev):
                ev.accept()

    def raiseContextMenu(self, ev):
        menu = self.getContextMenus()

        # Let the scene add on to the end of our context menu
        # (this is optional)
        menu = self.scene().addParentContextMenus(self, menu, ev)

        pos = ev.screenPos()
        menu.popup(QtCore.QPoint(pos.x(), pos.y()))
        return True

    # This method will be called when this item's _children_ want to raise
    # a context menu that includes their parents' menus.
    def getContextMenus(self, event=None):
        if self.menu is None:
            self.menu = QtGui.QMenu()
            self.menu.setTitle(self.name+ " options..")

            green = QtGui.QAction("Turn green", self.menu)
            green.triggered.connect(self.setGreen)
            self.menu.addAction(green)
            self.menu.green = green

            blue = QtGui.QAction("Turn blue", self.menu)
            blue.triggered.connect(self.setBlue)
            self.menu.addAction(blue)
            self.menu.green = blue

            alpha = QtGui.QWidgetAction(self.menu)
            alphaSlider = QtGui.QSlider()
            alphaSlider.setOrientation(QtCore.Qt.Horizontal)
            alphaSlider.setMaximum(255)
            alphaSlider.setValue(255)
            alphaSlider.valueChanged.connect(self.setAlpha)
            alpha.setDefaultWidget(alphaSlider)
            self.menu.addAction(alpha)
            self.menu.alpha = alpha
            self.menu.alphaSlider = alphaSlider
        return self.menu

    # Define context menu callbacks
    def setGreen(self):
        self.pen = pg.mkPen('g')
        # inform Qt that this item must be redrawn.
        self.update()

    def setBlue(self):
        self.pen = pg.mkPen('b')
        self.update()

    def setAlpha(self, a):
        self.setOpacity(a/255.)


# This box's context menu will include the ViewBox's menu
box1 = MenuBox("Menu Box #1")
view.addItem(box1)

# This box's context menu will include both the ViewBox's menu and box1's menu
box2 = MenuBox("Menu Box #2")
box2.setParentItem(box1)
box2.setPos(5, 5)
box2.scale(0.2, 0.2)

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

        '''
#from PyQt4.QtCore import QCoreApplication, Qt
#from PyQt4.QtGui import QListWidget, QListWidgetItem, QApplication
#
#import sys
#
#class MyList(QListWidget):
#    def __init__(self):
#        QListWidget.__init__(self)
#        self.add_items()
#        self.itemClicked.connect(self.item_click)
#
#    def add_items(self):
#        for item_text in ['item1', 'item2', 'item3']:
#            item = QListWidgetItem(item_text)
#            self.addItem(item)
#
#    def item_click(self, item):
#        print item, str(item.text())
#
#if __name__ == '__main__':
#    app = QApplication([])
#    myList = MyList()
#    myList.show()
#    sys.exit(app.exec_())
#

''' ~~ INSERE / REMOVE BOTOES
from PyQt4 import QtGui, QtCore
import sys

class Main(QtGui.QMainWindow):
    def __init__(self, parent = None):
        super(Main, self).__init__(parent)

        # main button
        self.addButton = QtGui.QPushButton('button to add other widgets')
        self.addButton.clicked.connect(self.addWidget)

        # scroll area widget contents - layout
        self.scrollLayout = QtGui.QFormLayout()

        # scroll area widget contents
        self.scrollWidget = QtGui.QWidget()
        self.scrollWidget.setLayout(self.scrollLayout)

        # scroll area
        self.scrollArea = QtGui.QScrollArea()
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setWidget(self.scrollWidget)

        # main layout
        self.mainLayout = QtGui.QVBoxLayout()

        # add all main to the main vLayout
        self.mainLayout.addWidget(self.addButton)
        self.mainLayout.addWidget(self.scrollArea)

        # central widget
        self.centralWidget = QtGui.QWidget()
        self.centralWidget.setLayout(self.mainLayout)

        # set central widget
        self.setCentralWidget(self.centralWidget)

    def addWidget(self):
        self.scrollLayout.addRow(TestButton())


class TestButton(QtGui.QPushButton):
    def __init__( self, parent=None):
        super(TestButton, self).__init__(parent)
        self.setText("I am in Test widget")
        self.clicked.connect(self.deleteLater)


app = QtGui.QApplication(sys.argv)
myWidget = Main()
myWidget.show()
app.exec_() '''