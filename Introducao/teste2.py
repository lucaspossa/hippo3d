# -*- coding: utf-8 -*-
"""
Example demonstrating a variety of scatter plot features.
"""



### Add path to library (just for examples; you do not need this)
#import initExample

from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
import numpy as np

app = QtGui.QApplication([])
mw = QtGui.QMainWindow()
mw.resize(800,800)
view = pg.GraphicsLayoutWidget()  ## GraphicsView with GraphicsLayout inserted by default
mw.setCentralWidget(view)
mw.show()
mw.setWindowTitle('pyqtgraph example: ScatterPlot')

## create four areas to add plots
w1 = view.addPlot()
w2 = view.addViewBox()
w2.setAspectLocked(True)
view.nextRow()
w3 = view.addPlot()
w4 = view.addPlot()
print("Generating data, this takes a few seconds...")

## There are a few different ways we can draw scatter plots; each is optimized for different types of data:


## 1) All spots identical and transform-invariant (top-left plot).
## In this case we can get a huge performance boost by pre-rendering the spot
## image and just drawing that image repeatedly.

n = 300
s1 = pg.ScatterPlotItem(size=10, pen=pg.mkPen(None), brush=pg.mkBrush(255, 255, 255, 120))
pos = np.random.normal(size=(2,n), scale=1e-5)
spots = [{'pos': pos[:,i], 'data': 1} for i in range(n)] + [{'pos': [0,0], 'data': 1}]
s1.addPoints(spots)
w1.addItem(s1)

## Make all plots clickable
lastClicked = []
def clicked(plot, points):
    global lastClicked
    for p in lastClicked:
        p.resetPen()
    print("clicked points", points)
    for p in points:
        p.setPen('b', width=2)
    lastClicked = points
s1.sigClicked.connect(clicked)



## 2) Spots are transform-invariant, but not identical (top-right plot).
## In this case, drawing is almsot as fast as 1), but there is more startup
## overhead and memory usage since each spot generates its own pre-rendered
## image.

s2 = pg.ScatterPlotItem(size=10, pen=pg.mkPen('w'), pxMode=True)
pos = np.random.normal(size=(2,n), scale=1e-5)
spots = [{'pos': pos[:,i], 'data': 1, 'brush':pg.intColor(i, n), 'symbol': i%5, 'size': 5+i/10.} for i in range(n)]
s2.addPoints(spots)
w2.addItem(s2)
s2.sigClicked.connect(clicked)


## 3) Spots are not transform-invariant, not identical (bottom-left).
## This is the slowest case, since all spots must be completely re-drawn
## every time because their apparent transformation may have changed.

s3 = pg.ScatterPlotItem(pxMode=False)   ## Set pxMode=False to allow spots to transform with the view
spots3 = []
for i in range(10):
    for j in range(10):
        spots3.append({'pos': (1e-6*i, 1e-6*j), 'size': 1e-6, 'pen': {'color': 'w', 'width': 2}, 'brush':pg.intColor(i*10+j, 100)})
s3.addPoints(spots3)
w3.addItem(s3)
s3.sigClicked.connect(clicked)


## Test performance of large scatterplots

s4 = pg.ScatterPlotItem(size=10, pen=pg.mkPen(None), brush=pg.mkBrush(255, 255, 255, 20))
pos = np.random.normal(size=(2,10000), scale=1e-9)
s4.addPoints(x=pos[0], y=pos[1])
w4.addItem(s4)
s4.sigClicked.connect(clicked)



## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()


'''
#from PyQt5 import QtCore, QtWidgets
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np


class MyWidget(pg.GraphicsWindow):

    def __init__(self, parent=None):
        pg.GraphicsWindow.__init__(self, parent)
#        super().__init__(parent=parent)

        self.mainLayout = QtGui.QVBoxLayout()
        self.setLayout(self.mainLayout)

        self.timer = QtCore.QTimer(self)
        self.timer.setInterval(100) # in milliseconds
        self.timer.start()
        self.timer.timeout.connect(self.onNewData)

        self.plotItem = self.addPlot(title="Lidar points")

        self.plotDataItem = self.plotItem.plot([], pen=None,
            symbolBrush=(255,0,0), symbolSize=5, symbolPen=None)


    def setData(self, x, y):
        self.plotDataItem.setData(x, y)


    def onNewData(self):
        numPoints = 1000
        x = np.random.normal(size=numPoints)
        y = np.random.normal(size=numPoints)
        self.setData(x, y)


def main():
    app = QtGui.QApplication([])

    pg.setConfigOptions(antialias=False) # True seems to work as well

    win = MyWidget()
    win.show()
    win.resize(800,600)
    win.raise_()
    app.exec_()

if __name__ == "__main__":
    main()'''

'''# -*- coding: utf-8 -*-
from sys import argv#, exit
import pyqtgraph as pg
from pyqtgraph import QtGui as qg, QtCore as qc
import numpy as np
clicks = []
lista_x = []
lista_y = []

def retorna_coordenadas(ev):
    x_pos = ev.pos().x()
    y_pos = ev.pos().y()
    xc = np.ceil(ev.pos().x())
    xf = np.floor(ev.pos().x())
    yc = np.ceil(ev.pos().y())
    yf = np.floor(ev.pos().y())
    if (xc-x_pos) < (x_pos-xf):
        x = xc
    else:
        x = xf
    if (yc-y_pos) < (y_pos-yf):
        y = yc
    else:
        y = yf
    return [x,y]

class HippoView(pg.GraphicsView):
    def __init__(self):
        pg.GraphicsView.__init__(self)
        self.vb = pg.ViewBox()
        self.vb.setAspectLocked()
        self.setCentralItem(self.vb)
        self.grid = pg.GridItem()
        self.vb.addItem(self.grid)
        self.vb.setYRange(-100,500)
        self.scene().sigMouseClicked.connect(self.onClick)

    def onClick(self, ev):
        ev.currentItem = self.grid
        global clicks
        coords = retorna_coordenadas(ev)

        if len(clicks)==0:  # First mouse click - ONLY register coordinates
            clicks.append((coords[0],coords[1]))
            print("First click!", clicks)
        elif len(clicks)==1:  # Second mouse click - register coordinates of second click
            clicks.append((coords[0],coords[1]))
            print("Second click...", clicks)
            # Draw line connecting the two clicks
            print("...drawing line")
            line = pg.LineSegmentROI(clicks, pen=(4,9), snapSize = 1, translateSnap=True, scaleSnap=True)
#            line.
            self.vb.addItem(line)
            # reset clicks array
            clicks[:] = [] # this resets the *content* of clicks without changing the object itself
            self.scene().sigMouseClicked.disconnect()
        else:  # something went wrong, just reset clicks
            clicks[:] = []
            self.scene().sigMouseClicked.disconnect()

# -------------------------------------------------------------------------- #
def main():
    app = qg.QApplication(argv)
    win = qg.QMainWindow()
    win.setWindowState(qc.Qt.WindowMaximized)
    win.setWindowTitle('Cena do add')
    view = HippoView()
    win.setCentralWidget(view)
    win.show()
    app.exec_()
if __name__ == '__main__':
    main()'''

'''
    def niveis_aplica(self):
        # Cria a lista de níveis para uso no cálculo --------------------------
        var.gui_lista_niveis = []
        for i in range(10):
            try:
                nome = self.relacao_niveis.item(i, 0).text()
                cota = float(self.relacao_niveis.item(i, 1).text())
                var.gui_lista_niveis.append([nome, cota])
            except: break
        # Zera a árvore de níveis QTreeWidget ---------------------------------
        for i in range(10):
            try:
                self.arvore.removeChild(self.lista_niveis[i][0])
            except: break
        try: self.itemClicked.disconnect()
        except: pass
        self.lista_niveis = []
        # Cria a árvore de níveis QTreeWidget ---------------------------------
        for i in range(len(var.gui_lista_niveis)):
            if i == 0:
                self.lista_niveis.append(self.cria_nivel_fundacao())
                self.arvore.addChild(self.lista_niveis[i][0])
            else:
                self.lista_niveis.append(self.cria_nivel(var.gui_lista_niveis[i][0]))
                self.arvore.addChild(self.lista_niveis[i][0])
        self.expandAll()
        self.itemClicked.connect(self.onItemClicked)
        # Cria lista de canvas necessários ------------------------------------
        for i in range(len(var.gui_lista_niveis)):
            self.lista_canvas.append(HippoView(var.gui_lista_niveis[i][0]))

    def cria_nivel(self, nivel):
        lista = []
        item = qg.QTreeWidgetItem([nivel, 'Elementos', 'Cvs'])
        for i in range(4):
            if i == 0:
                lista.append(qg.QTreeWidgetItem([_tr('Nós'), '', '  +']))
            if i == 1:
                lista.append(qg.QTreeWidgetItem(['Pilares', '', '  +']))
            if i == 2:
                lista.append(qg.QTreeWidgetItem(['Vigas', '', '  +']))
            if i == 3:
                lista.append(qg.QTreeWidgetItem(['Lajes', '', '  +']))
            item.addChild(lista[i])
        return [item, lista]

    def cria_nivel_fundacao(self):
        lista = []
        item = qg.QTreeWidgetItem([_tr('Fundação'), 'Elementos', 'Cvs'])
        lista.append(qg.QTreeWidgetItem([_tr('Nós'), '', '  +']))
        lista.append(qg.QTreeWidgetItem(['Elementos', '', '  +']))
        item.addChild(lista[0])
        item.addChild(lista[1])
        return [item, lista]

    def cria_mdi_cad(self, mdi):
        self.cad = mdi
        self.lista_canvas = []

    @qc.pyqtSlot(qg.QTreeWidget, int)
    def onItemClicked(self, item, col):
        if item.parent() == None:
            return
        else: nivel = item.parent().text(0)

        if item.text(col) == 'Cvs':
            self.abre_cad(item.text(0))
        elif item.text(col) == '  +':
            print nivel
            print item.text(0)

        else: pass

        print 'rodada', self.teste
        self.teste += 1

        for i in var.gui_lista_niveis:
            print i
        for i in self.lista_niveis:
            print i

#        for i in range(len(self.lista_niveis)):
#            print 'lista niveis i0 =', self.lista_niveis[i][0].text(col)
#            for j in range(len(self.lista_niveis[i][1])):
#                print 'lista niveis i1 =', self.lista_niveis[i][1][j].text(col)

    def abre_cad(self, nivel):
        print 'Nivel CAD =', nivel
        sub = qg.QMdiSubWindow()
        canvas = HippoView(nivel)
        sub.setWidget(canvas)
        self.cad.addSubWindow(sub)
        sub.show()

        '''

'''

def main():
    app = qg.QApplication(argv)
    win = qg.QMainWindow()
    win.setWindowState(qc.Qt.WindowMaximized)
    win.setWindowTitle('Cena do add')
    view = HippoView()
    win.setCentralWidget(view)
    win.show()
    app.exec_()
if __name__ == '__main__':
    main()



class HPViewBox(pg.ViewBox):
    def __init__(self):
        pg.ViewBox.__init__(self)
        self.setAspectLocked()
        self.addItem(pg.GridItem())

        self.scene().sigMouseClicked.connect(self.onClick)

    def onClick(self, ev):
        global clicks
        x = ev.pos().x()
        y = ev.pos().y()
        if len(clicks)==0:  # First mouse click - ONLY register coordinates
            print("First click!")
            clicks.append((x,y))
        elif len(clicks)==1:  # Second mouse click - register coordinates of second click
            print("Second click...")
            clicks.append((x,y))

            # Draw line connecting the two clicks
            print("...drawing line")
            line = pg.LineSegmentROI(clicks, pen=(4,9))
            self.vb.addItem(line)

            # reset clicks array
            clicks[:] = [] # this resets the *content* of clicks without changing the object itself
        else:  # something went wrong, just reset clicks
            clicks[:] = []

def main():
    app = qg.QApplication(argv)
    win = qg.QMainWindow()
    win.setWindowState(qc.Qt.WindowMaximized)
    win.setWindowTitle('Cena do add')
    view_box = HPViewBox()
    hp_graph_view = pg.GraphicsView()
    hp_graph_view.setCentralWidget(view_box)

#    hp_graph_view.setM

    win.setCentralWidget(hp_graph_view)
    win.show()
    app.exec_()
if __name__ == '__main__':
    main()


'''







'''


        r3a = pg.ROI([2.5,2.5], [5,5])



        self.addItem(r3a)

        print r3a.pos()
        print r3a.size()

#        ## handles scaling horizontally around center
        r3a.addScaleHandle([1, 0.5], [0.5, 0.5])
        r3a.addScaleHandle([0, 0.5], [0.5, 0.5])

#        ## handles scaling vertically from opposite edge
        r3a.addScaleHandle([0.5, 0], [0.5, 1])
        r3a.addScaleHandle([0.5, 1], [0.5, 0])
#
        ## handles scaling both vertically and horizontally
        r3a.addScaleHandle([1, 1], [0, 0])
        r3a.addScaleHandle([0, 0], [1, 1])

        print r3a.viewPos()
#        self.



class PlotWdgt(pg.PlotWidget):
    def __init__(self):
        pg.PlotWidget.__init__(self)

class ViewWdgt(qg.QWidget):
    def __init__(self):
        qg.QWidget.__init__(self)

        # Cena, Graphics View e layout ----------------------------------------
        self.scene = qg.QGraphicsScene()
        self.view = pg.GraphicsView(self.scene)
#        self.view = qg.QGraphicsView(self.scene)
        self.set_layout()


        # Itens ---------------------------------------------------------------
        grid = pg.GridItem()
        self.vb = pg.ViewBox()
#        self.vb.addItem(grid)
#        self.vb.setAspectLocked()

#        self.teste = self.scene.


        # Add Itens to Scene --------------------------------------------------
#        self.scene.addItem(self.vb)
#        self.scene.addRect()


        # Add Itens to Graphics View ------------------------------------------
#        self.view.add(self.vb)
        self.view.enableMouse()
        self.view.setAspectLocked(True)
        self.view.addItem(self.vb)
        self.view.addItem(grid)


    def set_layout(self):
        # Layout QWidget ------------------------------------------------------
        lyt = qg.QVBoxLayout()
        lyt.addWidget(self.view)
        self.setLayout(lyt)
'''