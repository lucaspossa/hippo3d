# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore
import sys

class GxBlock(QtGui.QGraphicsItem):
    def __init__(self, scene, parent=None):
        ''' (QGraphicsScene, QGraphicsItem) -> NoneType
        '''
        QtGui.QGraphicsItem.__init__(self, parent, scene)

        self._text = 'Hello World'
        self._text_font_family = 'Verdana'
        self._text_font_size = 18
        self._text_color = 'white'
        self._background_color = 'blue'

        self._width, self._height = 200, 50

    def getStyle(self):
        ''' () -> {'text':str,
                   'text_font_family':str,
                   'text_font_size':int,
                   'text_color':str,
                   'background_color':str}

        Returns a dict with all the item style attributes.
        '''
        return {'text': self._text,
                'text_font_family': self._text_font_family,
                'text_font_size': self._text_font_size,
                'text_color': self._text_color,
                'background_color': self._background_color}

    def setStyle(self, **kwargs):
        ''' (kwargs={text:str,
                     text_font_family:str,
                     text_font_size:int,
                     text_color:str,
                     background_color:str}) -> NoneType

        Configures the item style attributes, shuch colors and fonts.
        '''
        self._text = kwargs.get('text', self._text)
        self._text_font_family = kwargs.get('text_font_family',
                                            self._text_font_family)
        self._text_font_size = kwargs.get('text_font_size',
                                          self._text_font_size)
        self._text_color = kwargs.get('text_color',
                                      self._text_color)
        self._background_color = kwargs.get('background_color',
                                            self._background_color)
        self.update()

    def getSize(self):
        ''' () -> QSize

        Return this item size, with attributes like 'width()' and 'height()'.
        '''
        return QtCore.QSize(self._width, self._height)

    def setSize(self, width, height):
        self.prepareGeometryChange()
        self._width, self._height = width, height
        self.update()

    def boundingRect(self):
        ''' QGraphicsItem.boundingRect() -> QRectF
        '''
        return QtCore.QRectF(0, 0, self._width, self._height)

    def paint(self, painter, option=None, widget=None):
        ''' QGraphicsItem.paint(QPainter, QStyleOptionGrpahicsItem,
                                QWidget) -> NoneType
        '''
        painter.fillRect(self.boundingRect(),
                         QtGui.QColor(self._background_color))
        painter.setPen(QtGui.QPen(QtGui.QColor(self._text_color)))
        painter.setFont(QtGui.QFont(self._text_font_family,
                                    self._text_font_size))
        painter.drawText(self.boundingRect(), QtCore.Qt.AlignCenter,
                         self._text)

class MyWindow(QtGui.QMainWindow):
    def __init__(self):
        ''' () -> NoneType
        '''
        QtGui.QMainWindow.__init__(self)
        self.initUI()

    def initUI(self):
        ''' () -> NoneType

        Creates the user interface.
        '''
        self.scene = QtGui.QGraphicsScene()
        self.scene.setSceneRect(0, 0, 1000, 1000)
        self.scene.setBackgroundBrush(QtGui.QColor(0, 255, 255))
#        self.scene.setBackgroundBrush(QtGui.QColor(230, 230, 230))

        self.gx_block = GxBlock(self.scene)
        self.gx_block.setPos(50, 50)
        self.gx_block.setFlags(QtGui.QGraphicsItem.ItemIsMovable |
                               QtGui.QGraphicsItem.ItemIsSelectable)

        ds = self.gx_block.getStyle()

        config_area = QtGui.QWidget(self)
        config_area.setFixedWidth(200)

        self.wg_lb_text = QtGui.QLabel('Text:', config_area)
        edit = self.wg_edit_text = QtGui.QLineEdit(config_area)
        edit.setText(ds['text'])
        self.connect(edit, QtCore.SIGNAL('textChanged(const QString&)'),
                     self.updateBlock)

        self.wg_lb_font_color = QtGui.QLabel('Font Color:')
        cbx = self.wg_cbx_font_color = QtGui.QComboBox(self)
        cbx.addItems(QtGui.QColor.colorNames())
        cbx.setCurrentIndex(cbx.findText(ds['text_color']))
        self.connect(cbx, QtCore.SIGNAL('currentIndexChanged(int)'),
                     self.updateBlock)

        self.wg_lb_font_family = QtGui.QLabel('Font Family:')
        cbx = self.wg_cbx_font_family = QtGui.QFontComboBox(self)
        cbx.setCurrentFont(QtGui.QFont(ds['text_font_family'],
                                       ds['text_font_size']))
        self.connect(cbx, QtCore.SIGNAL('currentIndexChanged(int)'),
                     self.updateBlock)

        self.wg_lb_font_size = QtGui.QLabel('Font Size:')
        spinbox = self.wg_spbx_font_size = QtGui.QSpinBox(self)
        spinbox.setValue(ds['text_font_size'])
        self.connect(spinbox, QtCore.SIGNAL('valueChanged(int)'),
                     self.updateBlock)

        self.wg_lb_background_color = QtGui.QLabel('Background Color:')
        combobox = self.wg_cbx_background_color = QtGui.QComboBox(self)
        combobox.addItems(QtGui.QColor.colorNames())
        combobox.setCurrentIndex(combobox.findText(ds['background_color']))
        self.connect(combobox, QtCore.SIGNAL('currentIndexChanged(int)'),
                     self.updateBlock)

        self.wg_lb_width = QtGui.QLabel('Width:')
        slider = self.wg_sld_width = QtGui.QSlider(self)
        slider.setRange(0, 400)
        slider.setOrientation(QtCore.Qt.Horizontal)
        slider.setValue(self.gx_block.getSize().width())
        self.connect(slider, QtCore.SIGNAL('valueChanged(int)'),
                     self.updateBlock)

        self.wg_lb_height = QtGui.QLabel('Height:')
        slider = self.wg_sld_height = QtGui.QSlider(self)
        slider.setRange(0, 400)
        slider.setOrientation(QtCore.Qt.Horizontal)
        slider.setValue(self.gx_block.getSize().height())
        self.connect(slider, QtCore.SIGNAL('valueChanged(int)'),
                     self.updateBlock)

        v_layout = QtGui.QVBoxLayout()
        v_layout.addWidget(self.wg_lb_text)
        v_layout.addWidget(self.wg_edit_text)
        v_layout.addWidget(self.wg_lb_font_color)
        v_layout.addWidget(self.wg_cbx_font_color)
        v_layout.addWidget(self.wg_lb_font_family)
        v_layout.addWidget(self.wg_cbx_font_family)
        v_layout.addWidget(self.wg_lb_font_size)
        v_layout.addWidget(self.wg_spbx_font_size)
        v_layout.addWidget(self.wg_lb_background_color)
        v_layout.addWidget(self.wg_cbx_background_color)
        v_layout.addWidget(self.wg_lb_width)
        v_layout.addWidget(self.wg_sld_width)
        v_layout.addWidget(self.wg_lb_height)
        v_layout.addWidget(self.wg_sld_height)
        v_layout.addStretch()
        config_area.setLayout(v_layout)

        # --- creating the main area ----------------------------------

        self.view = QtGui.QGraphicsView(self.scene, self)
        self.view.setRenderHint(QtGui.QPainter.Antialiasing)
        self.view.setRenderHint(QtGui.QPainter.TextAntialiasing)
        self.view.centerOn(0, 0)

        main_area = QtGui.QWidget(self)
        h_layout = QtGui.QHBoxLayout()
        h_layout.addWidget(self.view)
        h_layout.addWidget(config_area)
        main_area.setLayout(h_layout)

        self.setCentralWidget(main_area)
        self.setGeometry(200, 200, 640, 480)

    def updateBlock(self):

        self.gx_block.setStyle(
            text=self.wg_edit_text.text(),
            text_color=self.wg_cbx_font_color.currentText(),
            text_font_family=self.wg_cbx_font_family.currentText(),
            text_font_size=self.wg_spbx_font_size.value(),
            background_color=self.wg_cbx_background_color.currentText())

        self.gx_block.setSize(self.wg_sld_width.value(),
                              self.wg_sld_height.value())

def main():
    app = QtGui.QApplication(sys.argv)
    win = MyWindow()
    win.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()