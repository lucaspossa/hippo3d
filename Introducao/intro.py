# -*- coding: utf-8 -*-

'''
QtDesigner, para criar visualmente as interfaces gráficas;
Para abrir o QtDesigner no windows, digite designer no console.

Framework Graphics View, um super canvas com grande capacidade gráfica:
Gerenciamento de itens (drag and drop, colisão, seleção múltipla, etc);
Antialiasing, gradientes, e diversas opções de desenho;
Construtor de paths (caminhos);

Livro bom e completo:
http://www.qtrac.eu/pyqtbook.html (baixe aqui os exercícios do livro, inclusive tem vários de Drag and Drop).
Tutoriais:
http://zetcode.com/tutorials/pyqt4/
Referência de classes:
http://www.riverbankcomputing.co.uk/static/Docs/PyQt4/html/classes.html
'''

from PyQt4.QtGui import *
import sys

def main():
    app = QApplication(sys.argv) # precisa ser criado antes de mais nada! recebe argumentos da linha de comando
    win = QWidget()              # widget sem parent significa que é top-level, ou seja, uma janela
    win.show()                   # o primeiro widget sem parent a ser mostrado torna-se janela principal
    sys.exit(app.exec_())        # exec_() dispara o mainloop, e ao terminar, encerra-se o interpretador

if __name__ == '__main__':
    main()