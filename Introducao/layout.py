# -*- coding: utf-8 -*-

'''
QtGui: contém todos os widgets e suas classes bases, assim como gerenciadores
de widgets (como os layouts e containers), eventos relacionados a widgets,
etc, assim como várias utilidades.
Lembre-se que nem todo widget é necessariamente visível.
QtCore: além do importante submódulo QtCore.Qt que reúne diversas constantes
de PyQt, também apresenta vários objetos abstratos importantes como QRect,
QSize, QPoint, QDate, etc.
'''
from PyQt4 import QtGui, QtCore
import sys

def main():
    app = QtGui.QApplication(sys.argv)

    win = QtGui.QWidget()
    win.setGeometry(200, 200, 0, 0)
    win.setWindowTitle('Send Mail')

    # criando os widgets.. observe que todos possuem "win" 
    # como parent (último argumento)

    label_to = QtGui.QLabel('To', win)
    edit_to = QtGui.QLineEdit(win)

    label_subject = QtGui.QLabel('Subject', win)
    edit_subject = QtGui.QLineEdit(win)

    text_body = QtGui.QTextEdit(win)
    text_body.setMinimumHeight(200)
    text_body.setMinimumWidth(400)

    # os botões possuirão largura fixa
    btn_send = QtGui.QPushButton('Send', win)
    btn_send.setFixedWidth(100)
    btn_cancel = QtGui.QPushButton('Discard', win)
    btn_cancel.setFixedWidth(100)

    # este layout é uma grade bidimensional "infinita", onde cada
    # widget ocupa uma célula de coordenadas (x, y), onde x cresce
    # de cima para baixo, e y, da esquerda para a direita
    grid_layout = QtGui.QGridLayout()
    # parametros: widget, x, y, alinhamento
    grid_layout.addWidget(label_to, 0, 0, QtCore.Qt.AlignRight)
    grid_layout.addWidget(edit_to, 0, 1)
    grid_layout.addWidget(label_subject, 1, 0, QtCore.Qt.AlignRight)
    grid_layout.addWidget(edit_subject, 1, 1)

    # este layout é linear, onde os widgets são enfileirados da esquerda
    # para a direita, na ordem em que foram inseridos
    h_layout = QtGui.QHBoxLayout()
    h_layout.addStretch()
    h_layout.addWidget(btn_send)
    h_layout.addWidget(btn_cancel)

    # este layout é linear, onde os widgets são empilhados de cima para
    # baixo, na ordem em que foram inseridos
    # em especial, este layout vai conter os outros 2 criados anteriormente
    v_layout = QtGui.QVBoxLayout()
    v_layout.addLayout(grid_layout)
    v_layout.addWidget(text_body)
    v_layout.addLayout(h_layout)

    # configura o layout final da janela (que atua como container)
    win.setLayout(v_layout)
    win.show()

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()