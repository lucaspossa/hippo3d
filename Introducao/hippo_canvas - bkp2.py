# -*- coding: utf-8 -*-

from hippo_variaveis import np, pg, qg

clicks = []
class HippoView(qg.QWidget):
    def __init__(self, nivel, indice):
        qg.QWidget.__init__(self)
        self.nivel = nivel
        self.indice = indice
        print self.nivel, self.indice
        self.view = pg.GraphicsView()
        self.vb = pg.ViewBox()
        self.vb.setAspectLocked()
        self.view.setCentralItem(self.vb)
        self.grid = pg.GridItem()
        self.vb.addItem(self.grid)
        self.vb.setRange(yRange=(-100,500), xRange=(-100,500))
        self.bt1 = qg.QPushButton('BT')
        self.bt2 = qg.QPushButton('BT')
        self.bt3 = qg.QPushButton('BT')
        self.bt4 = qg.QPushButton('BT')

        hbox = qg.QHBoxLayout()
        hbox.addWidget(self.bt1)
        hbox.addWidget(self.bt2)
        hbox.addWidget(self.bt3)
        hbox.addWidget(self.bt4)

        vbox = qg.QVBoxLayout()
        vbox.addWidget(self.view)
        vbox.addLayout(hbox)
        self.setLayout(vbox)

        self.view.scene().sigMouseClicked.connect(self.onClick)

    def onClick(self, ev):
        ev.currentItem = self.grid
        global clicks
        coords = retorna_coordenadas(ev)

        if len(clicks)==0:  # First mouse click - ONLY register coordinates
            clicks.append((coords[0],coords[1]))
            print("First click!", clicks)
        elif len(clicks)==1:  # Second mouse click - register coordinates of second click
            clicks.append((coords[0],coords[1]))
            print("Second click...", clicks)
            # Draw line connecting the two clicks
            print("...drawing line")
            line = pg.LineSegmentROI(clicks, pen=(4,9), snapSize = 10., translateSnap=True, scaleSnap=True)
            self.vb.addItem(line)
            # reset clicks array
            clicks[:] = [] # this resets the *content* of clicks without changing the object itself
            self.view.scene().sigMouseClicked.disconnect()
        else: clicks[:] = []


def retorna_coordenadas(ev):
    x_pos = ev.pos().x()
    y_pos = ev.pos().y()
    xc = np.ceil(ev.pos().x())
    xf = np.floor(ev.pos().x())
    yc = np.ceil(ev.pos().y())
    yf = np.floor(ev.pos().y())
    if (xc-x_pos) < (x_pos-xf):
        x = xc
    else:
        x = xf
    if (yc-y_pos) < (y_pos-yf):
        y = yc
    else:
        y = yf
    return [x,y]



'''
from hippo_variaveis import np, pg
clicks = []
class HippoView(pg.GraphicsView):
    def __init__(self, nivel):
        pg.GraphicsView.__init__(self)
        self.setWindowTitle(nivel.decode('latin_1'))
        self.vb = pg.ViewBox()
        self.vb.setAspectLocked()
        self.setCentralItem(self.vb)
        self.grid = pg.GridItem()
        self.vb.addItem(self.grid)
        self.vb.setRange(yRange=(-100,500), xRange=(-100,500))
        self.scene().sigMouseClicked.connect(self.onClick)

    def onClick(self, ev):
        ev.currentItem = self.grid
        global clicks
        coords = retorna_coordenadas(ev)

        if len(clicks)==0:  # First mouse click - ONLY register coordinates
            clicks.append((coords[0],coords[1]))
            print("First click!", clicks)
        elif len(clicks)==1:  # Second mouse click - register coordinates of second click
            clicks.append((coords[0],coords[1]))
            print("Second click...", clicks)
            # Draw line connecting the two clicks
            print("...drawing line")
            line = pg.LineSegmentROI(clicks, pen=(4,9), snapSize = 10., translateSnap=True, scaleSnap=True)
            self.vb.addItem(line)
            # reset clicks array
            clicks[:] = [] # this resets the *content* of clicks without changing the object itself
            self.scene().sigMouseClicked.disconnect()


def retorna_coordenadas(ev):
    x_pos = ev.pos().x()
    y_pos = ev.pos().y()
    xc = np.ceil(ev.pos().x())
    xf = np.floor(ev.pos().x())
    yc = np.ceil(ev.pos().y())
    yf = np.floor(ev.pos().y())
    if (xc-x_pos) < (x_pos-xf):
        x = xc
    else:
        x = xf
    if (yc-y_pos) < (y_pos-yf):
        y = yc
    else:
        y = yf
    return [x,y]
'''