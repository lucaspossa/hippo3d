# -*- coding: utf-8 -*-
"""
Created on Sun Mar 10 20:22:15 2019

@author: Lucas
"""

import pyqtgraph as pg

class HippoCanvas(object):
    def __init__(self):
        self.my_plot = pg.PlotWidget()
        self.my_plot.showGrid(x=True, y=True, alpha=None)