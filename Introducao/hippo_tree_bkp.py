# -*- coding: utf-8 -*-

from hippo_variaveis import qg, _tr, qc, var
from hippo_canvas import HippoView

class HippoTree(qg.QTreeWidget):
    ''' Árvore de níveis '''
    def __init__(self):
        qg.QTreeWidget.__init__(self)
        self.setColumnCount(3)
        self.setHeaderLabels([_tr('Árvore de níveis'), 'Tabela', 'Add'])
        self.setColumnWidth(0, 250)
        self.setColumnWidth(1, 65)
        self.setColumnWidth(2, 25)
        self.setFixedWidth(350)

        # Tree-Árvore TopLevelItem ---------------------------------------------
        self.arvore = qg.QTreeWidgetItem([_tr('Níveis da estrutura')])
        self.addTopLevelItem(self.arvore)
        self.bt_tabela_niveis = qg.QPushButton(_tr('Níveis'))
        self.setItemWidget(self.arvore, 1, self.bt_tabela_niveis)
        self.bt_tabela_niveis.clicked.connect(self.dialogo_niveis)

    def dialogo_niveis(self):
        self.diag_niveis = qg.QDialog()
        # Botões ---------------------------------------------------------------
        cancelar = qg.QPushButton('Cancelar')
        self.connect(cancelar, qc.SIGNAL('clicked()'), self.diag_niveis.close)
        aplicar = qg.QPushButton('Aplicar')
        self.connect(aplicar, qc.SIGNAL('clicked()'), self.niveis_aplica)
        botao_ok = qg.QPushButton('OK')
        self.connect(botao_ok, qc.SIGNAL('clicked()'), self.niveis_ok)

        # Tabela ---------------------------------------------------------------
        self.relacao_niveis = qg.QTableWidget()
        self.relacao_niveis.setColumnCount(2)
        self.relacao_niveis.setRowCount(10)
        self.relacao_niveis.setColumnWidth(0,150)
        self.relacao_niveis.setColumnWidth(1,90)
        self.relacao_niveis.setHorizontalHeaderLabels(['Nome do pavimento', 'Cota (cm)'])
        self.relacao_niveis.setVerticalHeaderLabels([str(x+1) for x in range(10)])
        obj = qg.QLineEdit()
        for i in range(10):
            if i == 0:
                obj.setText(_tr('Fundação'))
                self.relacao_niveis.setItem(i, 0, qg.QTableWidgetItem(obj.text()))
            elif i == 1:
                obj.setText(_tr('Térreo'))
                self.relacao_niveis.setItem(i, 0, qg.QTableWidgetItem(obj.text()))
            else:
                obj.setText(_tr('Nível %d'%(i-1)))
                self.relacao_niveis.setItem(i, 0, qg.QTableWidgetItem(obj.text()))
            self.relacao_niveis.item(i,0).setFlags(qc.Qt.ItemIsEnabled)
        if var.gui_lista_niveis == []:
            obj.setText('0.0')
            self.relacao_niveis.setItem(0, 1, qg.QTableWidgetItem(obj.text()))
            obj.setText('150.0')
            self.relacao_niveis.setItem(1, 1, qg.QTableWidgetItem(obj.text()))
        else: self.preenche_dialogo_niveis()

        # Layout ---------------------------------------------------------------
        grid = qg.QGridLayout()
        grid.addWidget(self.relacao_niveis, 0,0,1,3)
        grid.addWidget(cancelar, 1,0)
        grid.addWidget(aplicar, 1,1)
        grid.addWidget(botao_ok, 1,2)

        self.diag_niveis.setLayout(grid)
        self.diag_niveis.setGeometry(300,100,290,390)
        self.diag_niveis.setWindowTitle(_tr("Níveis da estrutura"))
        self.diag_niveis.exec_()

    def preenche_dialogo_niveis(self):
        obj = qg.QLineEdit()
        for i in range(len(var.gui_lista_niveis)):
            obj.setText(str(var.gui_lista_niveis[i][1]))
            self.relacao_niveis.setItem(i, 1, qg.QTableWidgetItem(obj.text()))

    def niveis_ok(self):
        self.niveis_aplica()
        self.diag_niveis.close()

    def niveis_aplica(self):
        # Zera a árvore de níveis QTreeWidget ----------------------------------
        for i in range(10):
            try:
                self.arvore.removeChild(var.gui_lista_niveis[i][2])
            except: break
        try: self.itemClicked.disconnect()
        except: pass
        # Cria a lista de níveis para uso no cálculo ---------------------------
        var.gui_lista_niveis = []
        for i in range(10):
            try:
                nome = self.relacao_niveis.item(i, 0).text()
                cota = float(self.relacao_niveis.item(i, 1).text())
                var.gui_lista_niveis.append([nome, cota])
            except: break
        # Cria a árvore de níveis QTreeWidget ----------------------------------
        for i in range(len(var.gui_lista_niveis)):
            var.gui_lista_niveis[i].append(self.cria_nivel(var.gui_lista_niveis[i][0]))
            self.arvore.addChild(var.gui_lista_niveis[i][2])
        self.expandAll()
        self.itemClicked.connect(self.onItemClicked)
        # Cria lista de canvas necessários -------------------------------------
        self.lista_canvas = []
        for i in range(len(var.gui_lista_niveis)):
            self.lista_canvas.append(HippoView(var.gui_lista_niveis[i][0], i))

    def cria_nivel(self, nivel):
        if nivel == _tr('Fundação'):
            item = qg.QTreeWidgetItem([_tr('Fundação'), 'Elementos', 'Cvs'])
            item.addChild(qg.QTreeWidgetItem([_tr('Nós'), '', '  +']))
            item.addChild(qg.QTreeWidgetItem(['Elementos', '', '  +']))
            return item
        else:
            item = qg.QTreeWidgetItem([nivel, 'Elementos', 'Cvs'])
            item.addChild(qg.QTreeWidgetItem([_tr('Nós'), '', '  +']))
            item.addChild(qg.QTreeWidgetItem(['Pilares', '', '  +']))
            item.addChild(qg.QTreeWidgetItem(['Vigas', '', '  +']))
            item.addChild(qg.QTreeWidgetItem(['Lajes', '', '  +']))
            return item

    def cria_mdi_cad(self, mdi):
        self.cad = mdi
        self.lista_canvas = []

    @qc.pyqtSlot(qg.QTreeWidget, int)
    def onItemClicked(self, item, col):
        # Retorna nível e índice solicitado pelo usuário ao clicar no TreeWidget
        if item.parent() == None:
            return
        elif item.parent().text(0) == _tr("Níveis da estrutura"):
            nivel = item.text(0)
        else: nivel = item.parent().text(0)
        for i in range(10):
            try:
                if var.gui_lista_niveis[i][0] == nivel:
                    indice = i
            except: break
        nivel = unicode(nivel).encode('latin_1')
        # Conecta a coluna Canvas do TreeWidget para abertura do canvas no mdi -
        if item.text(col) == 'Cvs':
            self.abre_cad(nivel, indice)
        elif item.text(col) == '  +':
            self.lista_canvas[indice].clicks = []
            self.lista_canvas[indice].view.scene().sigMouseClicked.connect(self.lista_canvas[indice].onClick)
        else: pass

    def abre_cad(self, nivel, indice):
        sub = qg.QMdiSubWindow()
        sub.setWindowTitle(nivel)
        canvas = self.lista_canvas[indice]
        sub.setWidget(canvas)
        self.cad.addSubWindow(sub)
        sub.show()